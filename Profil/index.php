<!DOCTYPE html>
<?php 
   ob_start();
   session_start();
   include('../connect.php');
   $user= $_SESSION['user_mail'];
   $userId=$_GET['id'];
   
    date_default_timezone_set('Europe/Istanbul');
   
   function paylasilma_zamani ($time)
   {
   
       $time = time() - $time; // to get the time since that moment
       $time = ($time<1)? 1 : $time;
       $tokens = array (
           31536000 => 'yıl',
           2592000 => 'ay',
           604800 => 'hafta',
           86400 => 'gün',
           3600 => 'saat',
           60 => 'dakika',
           1 => 'saniye'
       );
   
       foreach ($tokens as $unit => $text) {
           if ($time < $unit) continue;
           $numberOfUnits = floor($time / $unit);
           return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
       }
   
   }
   
     ?>
<html lang="en">
   <?php include('../header.php');?>
   <style>
      .full {
      heigth: 100%;
      }
      @media (min-width: 768px) {
      /* show 3 items */
      .carousel-inner .active,
      .carousel-inner .active + .carousel-item,
      .carousel-inner .active + .carousel-item + .carousel-item {
      display: block;
      }
      .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
      .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
      .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
      transition: none;
      }
      .carousel-inner .carousel-item-next,
      .carousel-inner .carousel-item-prev {
      position: relative;
      transform: translate3d(0, 0, 0);
      }
      .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item {
      position: absolute;
      top: 0;
      right: -33.3333%;
      z-index: -1;
      display: block;
      visibility: visible;
      }
      /* left or forward direction */
      .active.carousel-item-left + .carousel-item-next.carousel-item-left,
      .carousel-item-next.carousel-item-left + .carousel-item,
      .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
      .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
      position: relative;
      transform: translate3d(-100%, 0, 0);
      visibility: visible;
      }
      /* farthest right hidden item must be abso position for animations */
      .carousel-inner .carousel-item-prev.carousel-item-right {
      position: absolute;
      top: 0;
      left: 0;
      z-index: -1;
      display: block;
      visibility: visible;
      }
      /* right or prev direction */
      .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
      .carousel-item-prev.carousel-item-right + .carousel-item,
      .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
      .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
      position: relative;
      transform: translate3d(100%, 0, 0);
      visibility: visible;
      display: block;
      visibility: visible;
      }
      }
      .emp-profile {
      padding: 3%;
      margin-top: 3%;
      margin-bottom: 3%;
      border-radius: 0.5rem;
      background: #fff;
      }
      .profile-img {
      text-align: center;
      }
      .profile-img img {
      width: 70%;
      height: 100%;
      }
      .profile-img .file {
      position: relative;
      overflow: hidden;
      margin-top: -20%;
      width: 70%;
      border: none;
      border-radius: 0;
      font-size: 15px;
      background: #212529b8;
      }
      .profile-img .file input {
      position: absolute;
      opacity: 0;
      right: 0;
      top: 0;
      }
      .profile-head h5 {
      color: #333;
      }
      .profile-head h6 {
      color: #0062cc;
      }
      .profile-edit-btn {
      border: none;
      border-radius: 1.5rem;
      width: 70%;
      padding: 2%;
      font-weight: 600;
      color: #6c757d;
      cursor: pointer;
      }
      .proile-rating {
      font-size: 12px;
      color: #818182;
      margin-top: 5%;
      }
      .proile-rating span {
      color: #495057;
      font-size: 15px;
      font-weight: 600;
      }
      .profile-head .nav-tabs {
      margin-bottom: 5%;
      }
      .profile-head .nav-tabs .nav-link {
      font-weight: 600;
      border: none;
      }
      .profile-head .nav-tabs .nav-link.active {
      border: none;
      border-bottom: 2px solid #0062cc;
      }
      .profile-work {
      padding: 14%;
      margin-top: -15%;
      }
      .profile-work p {
      font-size: 12px;
      color: #818182;
      font-weight: 600;
      margin-top: 10%;
      }
      .profile-work a {
      text-decoration: none;
      color: #495057;
      font-weight: 600;
      font-size: 14px;
      }
      .profile-work ul {
      list-style: none;
      }
      .profile-tab label {
      font-weight: 600;
      }
      .profile-tab p {
      font-weight: 600;
      color: #0062cc;
      }
   </style>
   <body class="full">
      <div class="full">
      <?php include('../navbar.php');?>
      <!-- lg => 2 + 4 + 4 + 2 = 12 -->
      <div class="container " id="aligncontent" style="margin-top:100px;">
         <?php 
            $user=$_SESSION['userNumber'];
            $sql = "SELECT * FROM user where id = '$userId' ";
            $result = mysqli_query($db,$sql);
            $row = mysqli_fetch_array($result) ; 
            
            ?>
         <div id="myTabContent" class="tab-content" style="margin-top: 25px;">
            <div class="tab-pane active in" id="home">
               <div class="container emp-profile">
                  <form method="post">
                     <div class="row">
                        <div class="col-md-4">
                           <div class="profile-img">
                              <img class=" img-fluid rounded-circle" src="../Kitaplık/Profilim/uploads/<?php echo $row['profilePictureUrl']; ?>" alt="" />
                           </div>
                        </div>
                        <div class="col-md-6" style="">
                           <div class="profile-head">
                              <?php 
                                 $sql="SELECT * FROM user WHERE id='$userId'";
                                 $result=mysqli_query($db,$sql);
                                  while($row=mysqli_fetch_array($result)){ ?>
                              <br>
                              <h5>
                                 <title> <?php echo $row['name']; ?></title>
                                 <?php echo $row['name']; ?>  <input type="submit" style="float:right" class="btn btn-sm btn-outline-info" name="btnAddMore" value="Takip Et" disabled/>
                              </h5>
                              <p class="proile-rating">Puanı : <span>8/10
                              <div class="row">
                                 <ul class="nav nav-pills col-lg-12 mb-2">
                                    <li class="nav-item active">
                                       <a class="nav-link active" data-toggle="pill" href="#menu1">Popüler Notlar</a>
                                    </li>
                                    <li class="nav-item">
                                       <a class="nav-link" data-toggle="pill" href="#menu2">Tüm Notlar</a>
                                    </li>
                                 </ul>
                                 <div class="tab-content"style="width:100%;">
                                    <div class="tab-pane   active" id="menu1">
                                       <?php 
                                          $sqlUser = "SELECT * FROM note where userId = '$userId' ORDER BY noteLike DESC  LIMIT 4  ";
                                          $resultUser = mysqli_query($db,$sqlUser);
                                          $count=mysqli_num_rows ($resultUser);
                                          
                                          if($count != 0){
                                          
                                          while($rowUser = mysqli_fetch_array($resultUser)){ $time = strtotime($rowUser['created_at']);
                                          
                                          ?> 
                                       <a href="../Anasayfa/notDetay?id=<?php echo $rowUser['id'];?>"style="display:block">
                                          <div class="card col-lg-6"style="float:left;width:100%;height:150px">
                                             <div class="card-body">
                                                <h5 class="card-title"><?php echo $rowUser['className']; ?></h5>
                                                <h6 class="card-subtitle mb-2 text-muted"><small><i class="far fa-sm fa-clock"></i>  <?php echo paylasilma_zamani($time).' önce'; ?>&nbsp 
                                                   <i class="fas  fa-eye"></i> <?php echo $rowUser['noteView']; ?></small>
                                                </h6>
                                                <p class="card-text"style="text-decoration:none;color:black">
                                                   <?php echo $rowUser['description']; ?>
                                                </p>
                                             </div>
                                          </div>
                                       </a>
                                       <?php } ?>
                                       <?php } else{ ?>
                                       <div class="card-body">
                                          <h6 class="card-subtitle mb-2 text-muted"> Henüz bir şey paylaşmamış.</h6>
                                       </div>
                                       <?php } ?>
                                    </div>
                                    <div id="menu2" class=" tab-pane fade">
                                       <br>
                                       <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Tüm Notlarda Ara" class="form-control mb-2" autocomplete="off">
                                       <div class="table-responsive scrollview">
                                          <table class="table table-hover table-striped "id="myTable">
                                             <tr  class="header">
                                                <th>Ders</th>
                                                <th>Açıklama</th>
                                                <th>Beğeni</th>
                                                <th>Görüntülenme</th>
                                             </tr>
                                             <?php 
                                                $sqlAll = "SELECT * FROM note where userId = '$userId' ORDER BY id DESC ";
                                                $resultAll = mysqli_query($db,$sqlAll);
                                                $countAll=mysqli_num_rows ($resultAll);
                                                
                                                if($countAll != 0){
                                                
                                                while($rowAll = mysqli_fetch_array($resultAll)){ 
                                                
                                                ?> 
                                             <tr class='clickable-row' data-href='../Anasayfa/notDetay?id=<?php echo $rowAll['id'];?>'style="cursor:pointer">
                                                <td><?php echo $rowAll['className'];?></td>
                                                <td><?php echo $rowAll['description'];?></td>
                                                <td style="text-align:center"><i class="fas fa-heart" style="color:#FF0000"></i> <?php echo $rowAll['noteLike'];?></td>
                                                <td style="text-align:center"><i class="fas fa-eye" style="color:#328CCC"></i> <?php echo $rowAll['noteView'];?></td>
                                             </tr>
                                             <?php }}?>
                                          </table>
                                       </div>
                                       <script>
                                          jQuery(document).ready(function($) {
                                          $(".clickable-row").click(function() {
                                          window.location = $(this).data("href");
                                          });
                                          });
                                       </script>
                                       <script>
                                          function myFunction() {
                                            // Declare variables 
                                            var input, filter, table, tr, td, i, txtValue;
                                            input = document.getElementById("myInput");
                                            filter = input.value.toUpperCase();
                                            table = document.getElementById("myTable");
                                            tr = table.getElementsByTagName("tr");
                                          
                                            // Loop through all table rows, and hide those who don't match the search query
                                            for (i = 0; i < tr.length; i++) {
                                              td = tr[i].getElementsByTagName("td")[0];
                                              if (td) {
                                                txtValue = td.textContent || td.innerText;
                                                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                                                  tr[i].style.display = "";
                                                } else {
                                                  tr[i].style.display = "none";
                                                }
                                              } 
                                            }
                                          }
                                       </script>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <hr>
                        <?php 
                           $sqlNote = "SELECT * FROM note where userId = '$userId'";
                           $resultNote = mysqli_query($db,$sqlNote);
                           $numberNote = mysqli_num_rows($resultNote);
                           
                           $sqlGED = "SELECT * FROM ged where userId = '$userId'";
                           $resultGED = mysqli_query($db,$sqlGED);
                           $numberGED = mysqli_num_rows($resultGED);
                           
                           $sqlGEDUser = "SELECT * FROM gedUser where userId = '$userId'";
                           $resultGEDUser  = mysqli_query($db,$sqlGEDUser );
                           $numberGEDUser  = mysqli_num_rows($resultGEDUser );
                           
                           $sqlNoy = "SELECT * FROM noy where userId = '$userId'";
                           $resultNoy  = mysqli_query($db,$sqlNoy );
                           $numberNoy  = mysqli_num_rows($resultNoy );
                           ?>
                        <?php } ?>
                  </form>
                  </div>
                  <div class="row"style="text-align:center;margin-top:3%">
                     <div class="col-lg-3 col-md-3 col-sm-3">
                        <div >
                           <h1 style="color:#328CCC"><?php echo $numberNote ?> Adet</h1>
                           <h5 style="color:#909090">Not Paylaştı</h5>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3">
                        <div >
                           <h1 style="color:#328CCC"><?php echo $numberGED ?> Adet</h1>
                           <h5 style="color:#909090">Ders Anlattı</h5>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3">
                        <div >
                           <h1 style="color:#328CCC"><?php echo $numberGEDUser ?> Adet</h1>
                           <h5 style="color:#909090">Derse Katıldı</h5>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3">
                        <div>
                           <h1 style="color:#328CCC"><?php echo $numberNoy ?> Adet</h1>
                           <h5 style="color:#909090">Tartışma Başlattı</h5>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script>
         $(document).ready(function() {
         $("#myCarousel").on("slide.bs.carousel", function(e) {
         var $e = $(e.relatedTarget);
         var idx = $e.index();
         var itemsPerSlide = 3;
         var totalItems = $(".carousel-item").length;
         
         if (idx >= totalItems - (itemsPerSlide - 1)) {
         var it = itemsPerSlide - (totalItems - idx);
         for (var i = 0; i < it; i++) {
         // append slides to end
         if (e.direction == "left") {
           $(".carousel-item")
             .eq(i)
             .appendTo(".carousel-inner");
         } else {
           $(".carousel-item")
             .eq(0)
             .appendTo($(this).find(".carousel-inner"));
         }
         }
         }
         });
         });
         
      </script>
   </body>
</html>