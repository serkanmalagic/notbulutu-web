<!DOCTYPE html>
<?php 
ob_start();
session_start();
include('../../connect.php');
$user= $_COOKIE['user_mail'];
$userId = $_COOKIE['userId'];
if(!$_COOKIE['userDepartmentId']){
    header("Location:/Anasayfa");
    }
date_default_timezone_set('Europe/Istanbul');

function paylasilma_zamani ($time)
{

    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'yıl',
        2592000 => 'ay',
        604800 => 'hafta',
        86400 => 'gün',
        3600 => 'saat',
        60 => 'dakika',
        1 => 'saniye'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
    }

}

  ?>
  <title>Profilim</title>
<html lang="en">
<?php include('../../header.php');?>
 <style>
        
        .full{
            heigth:100%;
        }
        
        @media (min-width: 768px) {
  /* show 3 items */
  .carousel-inner .active,
  .carousel-inner .active + .carousel-item,
  .carousel-inner .active + .carousel-item + .carousel-item {
    display: block;
  }

  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
    transition: none;
  }

  .carousel-inner .carousel-item-next,
  .carousel-inner .carousel-item-prev {
    position: relative;
    transform: translate3d(0, 0, 0);
  }

  .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item {
    position: absolute;
    top: 0;
    right: -33.3333%;
    z-index: -1;
    display: block;
    visibility: visible;
  }

  /* left or forward direction */
  .active.carousel-item-left + .carousel-item-next.carousel-item-left,
  .carousel-item-next.carousel-item-left + .carousel-item,
  .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
  .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
    position: relative;
    transform: translate3d(-100%, 0, 0);
    visibility: visible;
  }

  /* farthest right hidden item must be abso position for animations */
  .carousel-inner .carousel-item-prev.carousel-item-right {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    display: block;
    visibility: visible;
  }

  /* right or prev direction */
  .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
  .carousel-item-prev.carousel-item-right + .carousel-item,
  .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
  .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
    position: relative;
    transform: translate3d(100%, 0, 0);
    visibility: visible;
    display: block;
    visibility: visible;
  }
}









.emp-profile{
    padding: 3%;
    margin-top: 3%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
}
.profile-img{
    text-align: center;
}
.profile-img img{
    width: 70%;
    height: 100%;
}
.profile-img .file {
    position: relative;
    overflow: hidden;
    margin-top: -20%;
    width: 70%;
    border: none;
    border-radius: 0;
    font-size: 15px;
    background: #212529b8;
}
.profile-img .file input {
    position: absolute;
    opacity: 0;
    right: 0;
    top: 0;
}
.profile-head h5{
    color: #333;
}
.profile-head h6{
    color: #0062cc;
}
.profile-edit-btn{
    border: none;
    border-radius: 1.5rem;
    width: 70%;
    padding: 2%;
    font-weight: 600;
    color: #6c757d;
    cursor: pointer;
}
.proile-rating{
    font-size: 12px;
    color: #818182;
    margin-top: 5%;
}
.proile-rating span{
    color: #495057;
    font-size: 15px;
    font-weight: 600;
}
.profile-head .nav-tabs{
    margin-bottom:5%;
}
.profile-head .nav-tabs .nav-link{
    font-weight:600;
    border: none;
}
.profile-head .nav-tabs .nav-link.active{
    border: none;
    border-bottom:2px solid #0062cc;
}
.profile-work{
    padding: 14%;
    margin-top: -15%;
}
.profile-work p{
    font-size: 12px;
    color: #818182;
    font-weight: 600;
    margin-top: 10%;
}
.profile-work a{
    text-decoration: none;
    color: #495057;
    font-weight: 600;
}
    font-size: 14px;
.profile-work ul{
    list-style: none;
}
.profile-tab label{
    font-weight: 600;
}
.profile-tab p{
    font-weight: 600;
    color: #0062cc;
}





    </style>
<body  class="full">
   
    
    
    
    
    
    
    
    
    
  
    <div class="full">
   
    <?php include('../../navbar.php');?>
  
    <!-- lg => 2 + 4 + 4 + 2 = 12 -->
    <div class="container full "id="aligncontent"style="margin-top:100px;">
                    	<?php 
                        	if($_GET['update']=="yes"){
                             echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                                      <strong><i class='fas fa-lg fa-user-circle'></i></strong> Profil bilgilerin başarıyla güncellendi.
                                      <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                        <span aria-hidden='true'>&times;</span>
                                      </button>
                                    </div>";   
                               }
                            elseif($_GET['status']=="samepwd"){
                                 echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                          <strong><i class='fas fa-lg fa-key'></i></strong> Girdiğin şifreler eşleşmiyor.
                                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>";   
                                   }
                            elseif($_GET['status']=="oldpwd"){
                                 echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                          <strong><i class='fas fa-lg fa-key'></i></strong> Eski şifren yanlış gibi görünüyor.
                                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>";   
                                   }     
                            elseif($_GET['status']=="success"){
                                 echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                                          <strong><i class='fas fa-lg fa-key'></i></strong> Şifre güncelleme başarılı.
                                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>";   
                                   }
                       ?>
 <nav>
                              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="nav-home" aria-selected="true">Profilim</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#mynotes" role="tab" aria-controls="nav-profile" aria-selected="false">Notlarım</a>
                                <!--<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#mygeds" role="tab" aria-controls="nav-profile" aria-selected="false">G.E.Dlerim</a>-->
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#changepassword" role="tab" aria-controls="nav-profile" aria-selected="false">Şifre Değiştir</a>
                                
<!--                                 <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="nav-profile" aria-selected="false">Ayarlar</a>
-->                                 
                              </div>
                            </nav>
                            <?php 
                            $user=$_COOKIE['userNumber'];
								$sql = "SELECT * FROM user where schoolnumber = '$user' ";
								$result = mysqli_query($db,$sql);
								$row = mysqli_fetch_array($result) ; 
										
								
							?>
                            <div id="myTabContent" class="tab-content" style="margin-top: 25px;">
                                    <div class="tab-pane active in" id="home">
                                        <div class="container emp-profile">
            
                <div class="row">
                    <div class="col-md-4">
                        <div class="profile-img">
                            <img class="img-thumbnail rounded-circle" width="45" src="uploads/<?php echo $row['profilePictureUrl']; ?>" alt=""/>
                        </div>
                    </div>
                    <div class="col-md-6"style="">
                        <div class="profile-head">
                              <?php 
                                 
                                $sql="SELECT * FROM user WHERE schoolnumber='$user'";
                                $result=mysqli_query($db,$sql);
                                 while($row=mysqli_fetch_array($result)){ 
                                    $did=$row['departmentId'];
                                    
                                    $sqlDepartment="SELECT * FROM department WHERE id='$did'";
                                    $resultDepartment=mysqli_query($db,$sqlDepartment);
                                    while($rowDepartment=mysqli_fetch_array($resultDepartment)){
                                        $fid = $rowDepartment['facultyId']?>
                                    
                                    <h5>
                                        <?php echo $row['name']; ?>
                                    </h5>
                                    <h6>
                                        <?php echo $row['department']; ?>
                                    </h6>
                                    <p class="proile-rating">Puanın : <span><?php echo $row['np']; ?>/20
                                 
                                      
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>E-Postan</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p> <?php echo $row['mail']; ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Telefonun</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p><?php echo $row['phoneNumber']; ?></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Bölümün</label>
                                            </div>
                                            <div class="col-md-6">
                                                <p> <?php echo $rowDepartment['departmentName']; ?></p>
                                            </div>
                                        </div>
                    </div>
                    </div>
                    <div class="col-md-2"style="text-align:center">
                        <input type="button" class="profile-edit-btn" name="btnAddMore" value="Düzenle" data-toggle="modal" data-target="#exampleModalCenter"/>
                    </div>
                    
                    <!-- Modal -->
                    <form action="update" method="POST" enctype="multipart/form-data">
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                      <div class="modal-dialog " role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Bilgilerin</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                              <div class="profile-img">
                                    <img class="rounded-circle img-thumbnail" width="45" src="uploads/<?php echo $row['profilePictureUrl']; ?>" alt=""/>
                                    
                              </div>
                              <div class="row">
                                  <div class="col-md-12">
                                               <input type="file" name="fileToUpload" id="fileToUpload">
                                            </div>
                              </div><br>
                            <div class="row">
                                            <div class="col-md-6">
                                                <label>E-Postan</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input ype="text" class="form-control" value="<?php echo $row['mail']; ?>"name="mail" />
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Telefonun</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input ype="text"name="phoneNumber" class="form-control" value="<?php echo $row['phoneNumber']; ?>"/>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Bölümün</label>
                                            </div>
                                            <div class="col-md-6">
                                                <select class=" form-control" name="department">
                                                    
                                        <option selected value="<?php echo $rowDepartment['id']; ?>">
                                            <?php echo $rowDepartment['departmentName']; ?>
                                            </option>
                                                    <?php
                                                     $sql="SELECT *  FROM department WHERE facultyId = '$fid'";
                                                     $result=mysqli_query($db,$sql); ?>
                                                    <?php while($row= mysqli_fetch_array($result)) { ?>
                                                        <option value="<?php echo $row['id']; ?>">
                                                            <?php echo $row['departmentName']; ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                          </div>
                          <div class="modal-footer">
                            
                            <button type="submit" class="btn btn-primary"name="submit">Değişiklikleri kaydet</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </form>
                    <!-- Modal END -->
                </div>
                <hr>
                <?php 
                    $sqlNote = "SELECT * FROM note where userId = '$userId'";
                    $resultNote = mysqli_query($db,$sqlNote);
                    $numberNote = mysqli_num_rows($resultNote);
                    
                    
                    $sqlGED = "SELECT * FROM ged where userId = '$userId'";
                    $resultGED = mysqli_query($db,$sqlGED);
                    $numberGED = mysqli_num_rows($resultGED);
                    
                    
                    $sqlGEDUser = "SELECT * FROM gedUser where userId = '$userId'";
                    $resultGEDUser  = mysqli_query($db,$sqlGEDUser );
                    $numberGEDUser  = mysqli_num_rows($resultGEDUser );
                    
                    
                    $sqlNoy = "SELECT * FROM noy where userId = '$userId'";
                    $resultNoy  = mysqli_query($db,$sqlNoy );
                    $numberNoy  = mysqli_num_rows($resultNoy );
                    ?>
                <div class="row"style="text-align:center;">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="profile-work">
                            <h1 style="color:#328CCC"><?php echo $numberNote ?> Kez</h1>
                            <h5 style="color:#909090">Not Paylaştı</h5>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="profile-work">
                            <h1 style="color:#328CCC"><?php echo $numberGED ?> Kez</h1>
                            <h5 style="color:#909090">Ders Anlattı</h5>
                            
                        </div>
                    </div>
                  <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="profile-work">
                            <h1 style="color:#328CCC"><?php echo $numberGEDUser ?> Kez</h1>
                            <h5 style="color:#909090">Derse Katıldı</h5>
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <div class="profile-work">
                            <h1 style="color:#328CCC"><?php echo $numberNoy ?> Kez</h1>
                            <h5 style="color:#909090">Tartışma Başlattı</h5>
                            
                        </div>
                    </div>
                        
                    
                </div>
                <?php }} ?>
            </form>           
        </div>
                                    </div>
                                    <div class="tab-pane fade" id="changepassword">

                                           <form action="changepassword" method="post">
                                                    <label>ESKI SIFRE</label>
                                                    <input name="oldpwd" type="password" class="input-xlarge form-control" autocomplete="off">



                                                    <label>YENI SIFRE</label>
                                                    <input type="password" name="newpwd"class="input-xlarge form-control"autocomplete="off">

                                                    <label>YENI SIFRE ( TEKRAR )</label>
                                                    <input type="password" name="newpwd2" class="input-xlarge form-control"autocomplete="off">
                                                    <div style="margin-top: 2%;">
                                                           <button type="submit" style="float:right" class="btn btn-primary">GÜNCELLE</button>
                                                    </div>
                                           </form>
                                    </div>
                                 
                                   <!-- <div class="tab-pane fade" id="settings">

                                           Ayarlar
                                    </div>-->
                                    
                                    <div class="tab-pane fade" id="mynotes">

                                          <table class="table table-striped" id="mytable">
                                                <thead class="bg-light">
                                                    <tr class="border-0">
                                                        <th class="border-0">Ders Adı</th>
                                                        <th class="border-0">Açıklama</th>
                                                        <th class="border-0"style="text-align:center">Beğeni</th>
                                                        <th class="border-0"style="text-align:center">Görüntülenme</th>
                                                        <th class="border-0">Kayıt Tarihi</th>
                                                        <th class="border-0">Güncelle</th>
                                                        <th class="border-0">Sil</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sql="Select * from note where userId = $userId";
                                                        $result=mysqli_query($db,$sql);
                                                        while($row= mysqli_fetch_array($result)){
                                                            $time = strtotime($row['created_at']);
                                                        ?>
                                                    <tr>
                                                        <td><?php echo $row['className']; ?> </td>
                                                        <td><?php echo $row['description']; ?></td>
                                                        <td style="text-align:center"><i class="fas fa-lg fa-heart"style="color:red"></i> <?php echo $row['noteLike']; ?></td>
                                                        <td style="text-align:center"><i class="fas fa-lg fa-eye"style="color:#328CCC"></i> <?php echo $row['noteView']; ?></td>
                                                        <td><i class="far fa-sm fa-clock"></i> <?php echo paylasilma_zamani($time).' önce'; ?></td>
                                                    
                                                        <td>
                                                        
                                                            
                                                            <button class="btn btn-sm btn-warning button-block"data-toggle="modal" data-target="#guncelle<?php echo $row['id']; ?>"><i class="far fa-edit"></i> Güncelle</button>
                                                        
                                                        </td>
                                                        <td><button class="btn btn-sm btn-outline-danger button-block"data-toggle="modal" data-target="#modal<?php echo $row['id']; ?>"><i class="far fa-trash-alt"></i> Sil</button></td>
                                                        
                                                    </tr>
                                                    <?php include("updateNoteModal.php");?>
                                                    
                                                    <!-- SiL Modal -->
                                                    
                                                    <div class="modal fade" id="modal<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel"><?php echo $row['className']; ?> - <?php echo $row['description']; ?>  notunu silmek üzeresiniz</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-footer">
                                                            <form action="./deleteNote" method="post">
                                                                <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                               
                                                                <button type="submit" class="btn btn-danger btn-block">Sil</button>
                                                            </form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    
                                                    
                                                    <?php } ?>
                                                    
                                                </tbody>
                                            </table>
                                    </div>
                                    
                                    
                                    
                                    
                                    <!-- G.E.D -->
                                    <!--<div class="tab-pane fade" id="mygeds">

                                          <table class="table table-striped" id="mytable">
                                                <thead class="bg-light">
                                                    <tr class="border-0">
                                                        <th class="border-0">Ders Adı</th>
                                                        <th class="border-0">Açıklama</th>
                                                        <th class="border-0"style="text-align:center"><i class="fas  fa-building"></i> Mekan</th>
                                                        <th class="border-0"style="text-align:center"><i class="fas  fa-clock"></i> Zaman</th>
                                                        <th class="border-0"style="text-align:center"><i class="fas fa-calendar-alt"></i> Tarih</th>
                                                        <th class="border-0">Kayıt Tarihi</th>
                                                        <th class="border-0">Güncelle</th>
                                                        <th class="border-0">Sil</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sql="Select * from ged where userId = $userId";
                                                        $result=mysqli_query($db,$sql);
                                                        while($row= mysqli_fetch_array($result)){
                                                            $time = strtotime($row['created_at']);
                                                        ?>
                                                    <tr>
                                                        <td><?php echo $row['lectureName']; ?> </td>
                                                        <td><?php echo $row['lectureDescription']; ?></td>
                                                        <td style="text-align:center"> <?php echo $row['meetingPlace']; ?></td>
                                                        <td style="text-align:center"> <?php echo $row['meetingTime']; ?></td>
                                                        <td style="text-align:center"> <?php echo $row['meetingDate']; ?></td>
                                                        <td><i class="far fa-sm fa-clock"></i> <?php echo paylasilma_zamani($time).' önce'; ?></td>
                                                    
                                                        <td>
                                                        
                                                            
                                                            <button class="btn btn-sm btn-warning button-block"data-toggle="modal" data-target="#guncelle<?php echo $row['id']; ?>"><i class="far fa-edit"></i> Güncelle</button>
                                                        
                                                        </td>
                                                        <td><button class="btn btn-sm btn-outline-danger button-block"data-toggle="modal" data-target="#modal<?php echo $row['id']; ?>"><i class="far fa-trash-alt"></i> İptal Et</button></td>
                                                        
                                                    </tr>
                                                   
                                                    
                                                    <!-- SiL Modal -->
                                                    
                                                   <!-- <div class="modal fade" id="modal<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel"><?php echo $row['lectureName']; ?> - <?php echo $row['lectureDescription']; ?>  G.E.Dini silmek üzeresiniz</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                            <div class="modal-body">
                                                                İşlem bedeli :  <span style="color:red"> -5 </span> <a href="../../np.php" title="notbulutu puanı">np</a><br>
                                                            </div>
                                                          <div class="modal-footer">
                                                            <form action="./deleteNote" method="post">
                                                                <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                               
                                                                <button type="submit" class="btn btn-danger btn-block">İptal Et</button>
                                                            </form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    
                                                    
                                                    <?php } ?>
                                                    
                                                </tbody>
                                            </table>
                                    </div>-->
                            </div>

 
    
    </div>
   
</div>
    <script>
        $(document).ready(function() {
  $("#myCarousel").on("slide.bs.carousel", function(e) {
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $(".carousel-item").length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction == "left") {
          $(".carousel-item")
            .eq(i)
            .appendTo(".carousel-inner");
        } else {
          $(".carousel-item")
            .eq(0)
            .appendTo($(this).find(".carousel-inner"));
        }
      }
    }
  });
});

    </script>
    
</body>
</html>

