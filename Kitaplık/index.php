<!DOCTYPE html>
<?php 
ob_start();
session_start();
$userId=$_COOKIE['userId'];
include('../connect.php');
if(!$_COOKIE['userDepartmentId']){
    header("Location:/Anasayfa");
    }
  ?>
  <title>Kitaplık</title>
<html lang="en">
<?php include('../header.php');?>
 <style>
        
        .full{
            heigth:100%;
        }
        
        @media (min-width: 768px) {
  /* show 3 items */
  .carousel-inner .active,
  .carousel-inner .active + .carousel-item,
  .carousel-inner .active + .carousel-item + .carousel-item {
    display: block;
  }

  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item,
  .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
    transition: none;
  }

  .carousel-inner .carousel-item-next,
  .carousel-inner .carousel-item-prev {
    position: relative;
    transform: translate3d(0, 0, 0);
  }

  .carousel-inner .active.carousel-item + .carousel-item + .carousel-item + .carousel-item {
    position: absolute;
    top: 0;
    right: -33.3333%;
    z-index: -1;
    display: block;
    visibility: visible;
  }

  /* left or forward direction */
  .active.carousel-item-left + .carousel-item-next.carousel-item-left,
  .carousel-item-next.carousel-item-left + .carousel-item,
  .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item,
  .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
    position: relative;
    transform: translate3d(-100%, 0, 0);
    visibility: visible;
  }

  /* farthest right hidden item must be abso position for animations */
  .carousel-inner .carousel-item-prev.carousel-item-right {
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    display: block;
    visibility: visible;
  }

  /* right or prev direction */
  .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
  .carousel-item-prev.carousel-item-right + .carousel-item,
  .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item,
  .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
    position: relative;
    transform: translate3d(100%, 0, 0);
    visibility: visible;
    display: block;
    visibility: visible;
  }
}

#myTable {
  border-collapse: collapse; /* Collapse borders */
  width:100%;
  border: 1px solid #ddd; /* Add a grey border */
  font-size: 18px; /* Increase font-size */
}

#myTable th, #myTable td {
  text-align: left; /* Left-align text */
  padding: 12px; /* Add padding */
}

#myTable tr {
  /* Add a bottom border to all table rows */
  border-bottom: 1px solid #ddd; 
  background-color:white;
}

#myTable tr.header, #myTable tr:hover {
  /* Add a grey background color to the table header and on hover */
  background-color: #f1f1f1;
}
.search {
  width: 130px;
  box-sizing: border-box;
  border: 2px solid #ccc;
  border-radius: 4px;
  font-size: 16px;
  background-color: white;
  
  background-position: 10px 10px; 
  background-repeat: no-repeat;
  padding: 12px 20px 12px 20px;
  -webkit-transition: width 0.4s ease-in-out;
  transition: width 0.4s ease-in-out;
}

.search:focus {
  width: 100%;
}



*[data-href] {
  cursor: pointer;
}
td a {
    color:black;
  display:inline-block;
  min-height:100%;
  width:100%;
  padding: 10px; /* add your padding here */
}
td {
  padding:0;  
}
    </style>
<body  class="full">
 
    <div class="full">
   
    <?php include('../navbar.php');?>
    
    <!-- lg => 2 + 4 + 4 + 2 = 12 -->
    <div class="container "id="aligncontent"style="margin-top:100px;">
	<h1>Kitaplığın</h1>
    <h3>Son Eklenenler<span style="float:right;"><a class="btn btn-lg btn-outline-secondary" href="../Kitaplık/Profilim"title="Profil"><i class="fas fa-lg fa-user-circle"></i> Profil</a></span></h3>
    
    <div class="row">
        
    <?php 
 
$sql="SELECT * FROM noteLibrary WHERE userId='$userId' ORDER BY id DESC LIMIT 3";
$result=mysqli_query($db,$sql);
$count=mysqli_num_rows($result);
if($count==0){
 echo "<div class='alert alert-info alert-dismissible fade show' role='alert'>
          <strong><i class='fas fa-lg fa-bookmark'></i></strong> Son eklenen notlarını bulamadık.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
}else{
 while($row=mysqli_fetch_array($result)){$noteId=$row['noteId'];
     $sqlNote="SELECT * FROM note WHERE id='$noteId' ";
    $resultNote=mysqli_query($db,$sqlNote);
    while($row=mysqli_fetch_array($resultNote)){?>
  <a class="card-link" href="../Anasayfa/notDetay.php?id=<?php echo $noteId ?>" style="color:#212529">
        <div class="card col-lg-4" style="width:100%">
		    <?php if($row['className']!=""){ ?>
		     <h5 class="card-title" style="padding-left:5%;padding-right:8%;padding-top:2%">
		          <?php echo mb_strtoupper ($row['className']);?><br><!--<span ><?php echo $row['created_at']?></span>--></h5>
		      <p style="padding-left:5%;padding-right:8%"><br><br><?php echo $row['description']?></p>
		          <?php }else{echo "<h5 class='card-title' style='padding-left:5%;padding-right:8%'>Başlıksız Belge</h5>";}?>
                            	 
                             	 <?php if($row['type']=="application/pdf" ){ ?>
                             	 <a href="../pdfs/<?php echo $row['name'];?>" target="_blank"><div class="alert alert-secondary"style="border-radius: 5px;"><h4 style="text-align: center"><i class="far fa-lg fa-file-pdf" title="pdf"></i>	 <?php echo $row['name'];?></h4>  </div>  </a>
                             	 <?php }else{?>
							<a href="../Anasayfa/notDetay?id=<?php echo $row['id'] ?>">	 <!--<img class="" style="width: 100%; "src="../images/<?php echo $row['name'];?>"> -->   </a>
							
                                 <?php } ?>
 
        </div>
    </a> 
      <?php }}} ?>
   
 </div>

        
     
    
    <hr>
  

  
  

<table id="myTable" class="table-striped"style="margin-bottom:5%">
  <tr class="header">
    <th style="width:100%"><input type="text"class="search" id="myInput" onkeyup="myFunction()" placeholder="Ara.."></th>
   
  </tr>
      <?php 
 
$sql="SELECT * FROM noteLibrary WHERE userId='$userId' ORDER BY id  DESC ";
$result=mysqli_query($db,$sql);
$count=mysqli_num_rows($result);
if($count==0){
 echo "<div class='alert alert-info alert-dismissible fade show' role='alert'>
          <strong><i class='fas fa-lg fa-bookmark'></i></strong> Kitaplığına biraz not ekleme vakti.
          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <span aria-hidden='true'>&times;</span>
          </button>
        </div>";
}else{
 while($row=mysqli_fetch_array($result)){$noteId=$row['noteId'];
     $sqlNote="SELECT * FROM note WHERE id='$noteId' ";
    $resultNote=mysqli_query($db,$sqlNote);
    while($row=mysqli_fetch_array($resultNote)){ ?>
   
  <tr>
       
    <td> <br>
    <a class="card-link" href="../Anasayfa/notDetay.php?id=<?php echo $noteId ?>">
    <b><?php echo mb_strtoupper ($row['className']);?></b><!--<span style="float:right;"><?php echo $row['created_at'];?></span>--><br><br>
    <?php echo $row['description'];?><br>
    </a>
    
    </td>
  </tr>
  

  <?php }}} ?>
</table>
<script>
function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
</script>
  
  <!--<div class="row">
      <div class="col-lg-4">
        <div class="card" style="width: 100%;">
      <img class="card-img-top" src="../profile.jpeg" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
      </div>
    </div>
      </div>
      <div class="col-lg-4">
        <div class="card" style="width: 100%;">
          <img class="card-img-top" src="../profile.jpeg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card" style="width: 100%;">
          <img class="card-img-top" src="../profile.jpeg" alt="Card image cap">
          <div class="card-body">
            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          </div>
        </div>
      </div>
    </div>-->
</div>
    
    
    </div>
</div>
  </div>
    <script>
        $(document).ready(function() {
            $('.carousel').carousel('pause');
  $("#myCarousel").on("slide.bs.carousel", function(e) {
       $('.carousel').carousel('pause');
    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 3;
    var totalItems = $(".carousel-item").length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
      var it = itemsPerSlide - (totalItems - idx);
      for (var i = 0; i < it; i++) {
        // append slides to end
        if (e.direction == "left") {
          $(".carousel-item")
            .eq(i)
            .appendTo(".carousel-inner");
        } else {
          $(".carousel-item")
            .eq(0)
            .appendTo($(this).find(".carousel-inner"));
        }
      
      }
      
    }
  });
});

    </script>
    
</body>
</html>

