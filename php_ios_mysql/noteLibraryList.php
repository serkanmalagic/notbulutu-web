<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    
    date_default_timezone_set('Europe/Istanbul');

    function paylasilma_zamani ($time)
    {
    
        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'yıl',
            2592000 => 'ay',
            604800 => 'hafta',
            86400 => 'gün',
            3600 => 'saat',
            60 => 'dakika',
            1 => 'saniye'
        );
    
        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
        }
    
    }
    
    //JSON FORMATINDAKİ VERİLERİ MOBİL EKRANIMIZA TAŞIMAYA YARAYACAKTIR.    
    define('DB_SERVER', 'localhost');
    define('DB_USERNAME', 'admin');
    define('DB_PASSWORD', 'parola098');
    define('DB_DATABASE', 'notbulutu');
    $con = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
    
    $userId = $_REQUEST["userId"];
    
    
    //USER BİLGİLERİ ALINDI
    $resultNoteLibrary = mysqli_query($con,"SELECT * FROM `noteLibrary` WHERE userId  = '$userId'") or die(mysqli_error($con));
    
    
    
    while($row1 = mysqli_fetch_array($resultNoteLibrary))
    {   
        $noteId = $row1['noteId'];
        
        //NOTE BİLGİLERİ ALINDI
        $resultNotes = mysqli_query($con,"SELECT * FROM `note` WHERE id  = '$noteId'") or die(mysqli_error($con));
        
        while($row2 = mysqli_fetch_array($resultNotes))
        {
            
            $noteId =               $row2['id'];
            $noteTitle =            $row2['className'];
            $notedescription =      $row2['description'];
            $noteLike =             $row2['noteLike'];
            $noteDislike =          $row2['noteDislike'];
            $noteamount =           $row2['amount'];
            $userIdNote =           $row2['userId'];
            $time =                 strtotime($row2['created_at']);
            $noteView =             $row2['noteView'];
            
            
            
            $sqlUserName= mysqli_query($con,"SELECT * FROM user WHERE id = '$userIdNote'");
            while($rowUserName = mysqli_fetch_array($sqlUserName))
            {
                $userName = $rowUserName['name'];
            }
            
            $sqlImage= mysqli_query($con,"SELECT * FROM image WHERE noteId = '$noteId' ORDER BY id DESC  LIMIT 1");
            while($rowImage = mysqli_fetch_array($sqlImage))
            {
                $noteArray[]=array(
                "noteId" =>$noteId,
                "noteTitle"=>$noteTitle,
                "noteDescription"=>$notedescription,
                "noteDislike"=>$noteDislike,
                "noteLike"=>$noteLike,
                "noteAmount"=>   $noteamount,
                "imageName"=>    $rowImage['name'],
                "documentType"=> $rowImage['type'],
                "imageSize"=>    $rowImage['size'],
                "userName" =>    $userName,
                "noteView" =>    $noteView,
                "time"     =>    paylasilma_zamani($time)
                );
               
            }
        }
        
    }
    
   

    
        
    echo json_encode($noteArray);

?>