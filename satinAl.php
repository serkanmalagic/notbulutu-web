<html>

    <head>
    
    
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


   
          <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
        
        <style>
  body {
  font-family: 'Open Sans', sans-serif;
  background: background: #00d2ff; background: -webkit-linear-gradient(to right, #00d2ff, #928dab);background: linear-gradient(to right, #00d2ff, #928dab);
  font-size: 16px;
}
.content {
  background-color: white;
  margin: 0px 30px;
  padding: 30px;
  border-radius: 10px;
	
}
body {
  font-family: Arial;
  font-size: 17px;
  padding: 8px;
}

* {
  box-sizing: border-box;
}

.row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  margin: 0 -16px;
}

.col-25 {
  -ms-flex: 25%; /* IE10 */
  flex: 25%;
}

.col-50 {
  -ms-flex: 50%; /* IE10 */
  flex: 50%;
}

.col-75 {
  -ms-flex: 75%; /* IE10 */
  flex: 75%;
}

.col-25,
.col-50,
.col-75 {
  padding: 0 16px;
}

.container {
  background-color: #f2f2f2;
  padding: 5px 20px 15px 20px;
  border: 1px solid lightgrey;
  border-radius: 3px;
}

input[type=text] {
  width: 100%;
  margin-bottom: 20px;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 3px;
}

label {
  margin-bottom: 10px;
  display: block;
}

.icon-container {
  margin-bottom: 20px;
  padding: 7px 0;
  font-size: 24px;
}

.btn {
  background-color: #4CAF50;
  color: white;
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 100%;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;
}

.btn:hover {
  background-color: #45a049;
}

a {
  color: #2196F3;
}

hr {
  border: 1px solid lightgrey;
}

span.price {
  float: right;
  color: grey;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other (also change the direction - make the "cart" column go on top) */
@media (max-width: 800px) {
  .row {
    flex-direction: column-reverse;
  }
  .col-25 {
    margin-bottom: 20px;
  }
}
        </style>
    
    <script></script>
    
    </head>
<body>
 <div style="height: 303px;background: #00d2ff; background: -webkit-linear-gradient(to right, #00d2ff, #928dab);background: linear-gradient(to right, #00d2ff, #928dab); center repeat-x;"><center><div style="padding-top: 3%;"><font size="+6" color="white"><strong>PAKET TUTARI</strong><br>20 TL</font> <br><font size="+2" color="white">not<strong>pay</strong><br></font></div></center></div>
 
 
 <div class="content">
  <div class="row">
  <div class="col-75">
    <div class="container">
      <form action="/action_page.php">
      
        <div class="row">
          <div class="col-50">
            <h3>Fatura Adresi</h3>
            <label for="fname"><i class="fa fa-user"></i> Ad Soyad</label>
            <input type="text" id="fname" name="firstname" placeholder="Ali Atılgan">
            <label for="email"><i class="fa fa-envelope"></i> Email</label>
            <input type="text" id="email" name="email" placeholder="ali@example.com">
            <label for="adr"><i class="fa fa-address-card-o"></i> Addres</label>
            <input type="text" id="adr" name="address" placeholder="">
            <label for="city"><i class="fa fa-institution"></i> İl</label>
            <input type="text" id="city" name="city" placeholder="İstanbul">

            <div class="row">
              <div class="col-50">
                <label for="state">İlçe</label>
                <input type="text" id="state" name="state" placeholder="Kadıköy">
              </div>
              <div class="col-50">
                <label for="zip">Posta Kodu</label>
                <input type="text" id="zip" name="zip" placeholder="34000">
              </div>
            </div>
          </div>

          <div class="col-50">
            <h3>Ödeme</h3>
            <label for="fname">Kabul Edilen Kartlar</label>
            <div class="icon-container">
              <i class="fa fa-cc-visa" style="color:navy;"></i>
              <i class="fa fa-cc-amex" style="color:blue;"></i>
              <i class="fa fa-cc-mastercard" style="color:red;"></i>
              <i class="fa fa-cc-discover" style="color:orange;"></i>
            </div>
            <label for="cname">Kartın Üzerindeki İsim</label>
            <input type="text" id="cname" name="cardname" placeholder="Ali Atılgan">
            <label for="ccnum">Kart Numarası</label>
            <input type="text" id="ccnum" name="cardnumber" placeholder="1111-2222-3333-4444">
            <label for="expmonth">Son Kullanma Tarihi / Ay</label>
            <input type="text" id="expmonth" name="expmonth" placeholder="Ağustos">
            <div class="row">
              <div class="col-50">
                <label for="expyear">Son Kullanma Tarihi / Yıl</label>
                <input type="text" id="expyear" name="expyear" placeholder="2018">
              </div>
              <div class="col-50">
                <label for="cvv">CVV</label>
                <input type="text" id="cvv" name="cvv" placeholder="352">
              </div>
            </div>
          </div>
          
        </div>
        
        <input type="submit" value="Ödeme Adımına Geç" class="btn">
       <button href="anasayfa.php" type="button" class="btn-lg btn-warning btn-block">İşlemi İptal Et</button>
      </form>
    </div>
  </div>

</div>

  
</div>


</body>

</html>

