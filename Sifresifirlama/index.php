<html>
    <?php include('../header.php');session_start();?>
    <body>
        <nav class="navbar navbar-expand-sm bg-light navbar-light justify-content-end">
    		<a class="navbar-brand" href="../"style=";font-size: 30px; color:#328CCC;"><b><img src="logo.png"width="62px"height="55px"/>notbulutu</b></a>
    		<button class="btn btn-outline-info ml-auto" data-toggle="modal" data-target="#loginModal" >Giriş Yap</button>
    	</nav>  
    	<?php 
                        	if($_GET['status']=="expired"){
                             echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                      <strong><i class='fas fa-lg fa-info-circle'></i></strong> Doğrulama maili geçersiz. Lütfen sıfırlama işlemini tekrar edin.
                                      <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                        <span aria-hidden='true'>&times;</span>
                                      </button>
                                    </div>";   
                               }
                            elseif($_GET['status']=="invalid"){
                                 echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                          <strong><i class='far fa-lg fa-envelope-open'></i></strong> Veriler bozulmuş. Lüften doğrulama linkine tekrar tıklayın.
                                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>";   
                                   }
                            elseif($_GET['status']=="success"){
                                 echo "<div class='alert alert-success alert-dismissible fade show' role='alert'>
                                          <strong><i class='far fa-lg fa-envelope'></i></strong> Şifre sıfırlama e-postasını <b>" .$_GET['mail']. " </b>adresine gönderdik. Mailindeki adımları takip edebilirsin.
                                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>";   
                                   }       
                            elseif($_GET['status']=="mail"){
                                 echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                          <strong><i class='far fa-lg fa-envelope-open'></i></strong> E-Posta adresini bulamadık. Lütfen tekrar dene.
                                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>";   
                                   }   
                            elseif($_GET['status']=="schoolnumber"){
                                 echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                          <strong><i class='far fa-lg fa-id-badge'></i></strong> Okul numaran doğru değil gibi görünüyor. Lütfen tekrar dene.
                                          <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                            <span aria-hidden='true'>&times;</span>
                                          </button>
                                        </div>";   
                                   }       
                       ?>
    	<div class="modal fade" id="loginModal" tabindex="-1" role="">
    <div class="modal-dialog modal-login" role="document">
        <div class="modal-content">
            <div class="card card-signup card-plain">
                <div class="modal-header">
                  
                        <b><img src="logo.png"width="62px"height="55px"style="margin-left: 15%;"/></b>
                    </div>
                </div>
                 <form class="form" method="post" action="../giris_yap" accept-charset="UTF-8" id="login-nav">
                <div class="modal-body">
                   
                        <div class="card-body">
                            
                               
                                   <label for="exampleInputEmail1">Okul Numarası</label> <br>
                                    <input type="text"id="exampleInputEmail1" class="form-control"name="user_number" aria-describedby="emailHelp"style="width:100%" required>
                                
                            
                           
                             
                                
                               <label for="exampleInputPassword1">Parola</label>
                                    <input type="password" class="form-control" id="exampleInputPassword1"name="user_password"required style="width:100%">
                                
                            <a href="./"style="float:left;">Şifremi unuttum</a>
                        </div>
                    
                </div>
                <div class="modal-footer">
                    
                    <button type="submit" class="btn btn-primary btn-info btn-wd btn-lg">Giriş Yap</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    	<div class="col-lg-4"></div>
    <div class="container col-lg-4" style= "margin-top: 5%;">

      <form class="form-signin"action="passwordreset"method="post">
          
        <h2 class="form-signin-heading">Şifre sıfırlama</h2>
        <label for="inputEmail" class="sr-only">Okul Numaran</label><br>
        <input type="text" id="inputEmail" name="schoolnumber" class="form-control" placeholder="Okul Numaran" required autofocus autocomplete="off">
        <label for="inputPassword" class="sr-only">E-Postan</label><br>
        <input type="email" id="inputPassword" name="user_mail" class="form-control" placeholder="E-Posta Adresin" required autocomplete="off"><br>
        <br>
        <button class="btn btn-lg  btn-block" style="background-color:#328CCC;color:white"type="submit">Şifremi Sıfırla</button>
      </form>

    </div> <!-- /container -->
<div class="col-lg-4"></div>

    </body>
</html>