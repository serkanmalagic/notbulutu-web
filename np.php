<?php include('header.php');?>
<?php include('connect.php')?>
<title>notbulutu Puanı</title>
<body>

    <?php include('navbar.php')?>
    <div class="col-lg-3"></div>
<div class="container col-lg-6"id="content"style="margin-top:90">
    	
   
    		<div class="card" style="width: 100%;">
      
      <div class="card-body">
        <h5 class="card-title">notbulutu Puanı</h5>
        
        <p class="card-text">notbulutu puanlarını, sistemi daha güvenilir hale getirmek ve kötü amaçlı kullanımların önüne geçmek için kullanıyoruz.<br>
        <h6 style="color:#328CCC">G.E.D</h6>
        Bu sayede bir kişi G.E.D oluşturduğunda, o kişinin gerçekten ders anlatmaya geleceğine güvenebilirsiniz.<br>
        
        Eğer G.E.D anlatıcısı sizseniz, buluşma mekanına gittiğinizde katılımcıların gelmediği bir manzara ile karşılaşma şansınızı düşük tutmaya çalışıyoruz.
        <br><br>
        <h6 style="color:#328CCC">Ders Notu</h6>
        Uygunsuz ders notu atanlara karşı halihazırda bir önlemimiz var ancak alakasız başlık ve/veya açıklama girilen notlara karşı raporlama sistemimiz ile bize bildirilenlerin uygunsuz bulunması durumunda
        notbulutu puanı bize yardımcı olacak.<br><br>

        <h4 class="card-title">Nasıl Çalışıyor ?</h4>
        <h5 class="card-title">Cezalar</h5>
        - G.E.D'e katılıp derse gitmemek. <span style="color:red">-10 notbulutu puanı</span><br>
        - G.E.D oluşturup ders anlatmaya gitmemek. <span style="color:red">-20 notbulutu puanı</span><br>
        - G.E.D oluşturup  ders tarihine 2 gün veya daha az kala düzenlemek. <span style="color:red">-2 notbulutu puanı</span><br>
        - G.E.D oluşturup ders tarihine 2 gün veya daha az kala iptal etmek.  <span style="color:red">-5 notbulutu puanı</span><br>
        - Uygunsuzluğu tespit edilen ders notu paylaşmak.  <span style="color:red">-1 notbulutu puanı</span><br><br>
        
        <h5 class="card-title">Ödüller</h5>
        - Ders Notu Paylaşmak. <span style="color:green">+1 notbulutu puanı</span><br>
        - G.E.D oluşturup ders anlatmayı başarıyla tamamlamak. <span style="color:green">+5 notbulutu puanı</span><br>
        - G.E.D'e katılmak. <span style="color:green">+2 notbulutu puanı</span><br>
        - Bir nOYunun hedefine ulaşması.  <span style="color:green">+5 notbulutu puanı</span><br>
        - Raporladığın bir içeriğe yaptırım uygulanması.  <span style="color:green">+1 notbulutu puanı</span><br>
        </p>
       <span style="color:#328CCC">notbulutu</span> Yönetim Ekibi.
      </div>
    </div>
</div>
<div class="col-lg-3"></div>