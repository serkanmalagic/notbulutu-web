    <?php date_default_timezone_set('Europe/Istanbul'); include('../../connect.php');?>
	<style>
	.dropzone .dz-preview .dz-progress .dz-upload { background: #328CCC;  }
	.navbar{
	    background-color:white;
	}
		.spinner {
			width: 40px;
			height: 40px;
			position: relative;
			margin: 100px auto;
		}
		
		.double-bounce1,
		.double-bounce2 {
			width: 100%;
			height: 100%;
			border-radius: 50%;
			background-color: #328ccc;
			opacity: 0.6;
			position: absolute;
			top: 0;
			left: 0;
			-webkit-animation: sk-bounce 2.0s infinite ease-in-out;
			animation: sk-bounce 2.0s infinite ease-in-out;
		}
		
		.double-bounce2 {
			-webkit-animation-delay: -1.0s;
			animation-delay: -1.0s;
		}
		
		@-webkit-keyframes sk-bounce {
			0%,
			100% {
				-webkit-transform: scale(0.0)
			}
			50% {
				-webkit-transform: scale(1.0)
			}
		}
		
		@keyframes sk-bounce {
			0%,
			100% {
				transform: scale(0.0);
				-webkit-transform: scale(0.0);
			}
			50% {
				transform: scale(1.0);
				-webkit-transform: scale(1.0);
			}
		}
		
		#myBtn {
			display: none;
			/* Hidden by default */
			position: fixed;
			/* Fixed/sticky position */
			bottom: 20px;
			/* Place the button at the bottom of the page */
			right: 30px;
			/* Place the button 30px from the right */
			z-index: 99;
			/* Make sure it does not overlap */
			border: none;
			/* Remove borders */
			outline: none;
			/* Remove outline */
			background-color: #328CCC;
			/* Set a background color */
			color: white;
			/* Text color */
			cursor: pointer;
			/* Add a mouse pointer on hover */
			padding: 15px;
			/* Some padding */
			border-radius: 10px;
			/* Rounded corners */
			font-size: 18px;
			/* Increase font size */
		}
		
		#myBtn:hover {
			background-color: white;
			/* Add a dark-grey background on hover */
			color: #328CCC;
		}
		
		.inputt {
			border: 2px solid #CCCCCC;
			border-radius: 8px 8px 8px 8px;
			margin-top: 1%;
			outline: medium none;
			padding: 8px 12px;
			width: 100%;
		}
		
		.zoom {
			transition: transform .2s;
			/* Animation */
			border-radius: 10px;
			margin: 0 auto;
		}
		
		.zoom:hover {
			transform: scale(1.3);
			/* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
		}
		
		#box {}
		
		li.aktif:hover {
			background-color: transparent;
		}
		
		.center {
			display: block;
			margin-left: auto;
			margin-right: auto;
			text-align: center;
			width: 100%;
		}
		/* Mobile Styles */
		/* Mobile Styles */
		
		@media only screen and (max-width: 400px) {
			body {
				background-color: #EEEEEE;
				/* Red */
			}
		}
		/* Tablet Styles */
		
		@media only screen and (min-width: 401px) and (max-width: 960px) {
			body {
				background-color: #EEEEEE;
				/* Yellow */
			}
			#content {
				margin-top: 10%;
			}
		}
		/* Desktop Styles */
		
		@media only screen and (min-width: 961px) {
			body {
				background-color: #EEEEEE;
				/* Blue */
			}
		}
		
		.scrollbox {
			height: auto;
			max-height:250px;
			overflow-y: scroll;
			overflow-x: hidden;
		}
		.scrollview {
			height: auto;
			max-height:400px;
			overflow-y: scroll;
			overflow-x: hidden;
		}
		

		
		
		
		
		
		
		
		
	
    /* Formatting search box */
    .search-box{
        width: 100%;
        position: relative;
        display: inline-block;
       
        
    }
    .search-box input[type="search"]{
      
        border: 1px solid #CCCCCC;
        
    }
    .result{
        position: absolute;        
        z-index: 999;
        top: 100%;
        left: 0;
         background-color:white;
         border-radius:5px;
         
    }
    .search-box input[type="search"], .result{
        width: 100%;
        box-sizing: border-box;
        
       
    }
    /* Formatting result items */
    .result p{
        margin: 0;
        padding: 7px 10px;
        border: 1px solid #CCCCCC;
        border-top: none;
        cursor: pointer;
    }
    .result p:hover{
        background: #f2f2f2;
    }
        
        
        
        
        
        /* Dropdown */
        
 /* -----------------------------------------
   Timeline
----------------------------------------- */
.timeline {
  list-style: none;
  padding-left: 0;
  position: relative;
  width:500px;
}
.timeline:after {
  content: "";
  height: auto;
  width: 1px;
  background: #e3e3e3;
  position: absolute;
  top: 5px;
  left: 30px;
  bottom: 25px;
}
.timeline.timeline-sm:after {
  left: 12px;
}
.timeline li {
  position: relative;
  padding-left: 70px;
  margin-bottom: 20px;
}
.timeline li:after {
  content: "";
  width: 12px;
  height: 12px;
  border-radius: 50%;
  background: #e3e3e3;
  position: absolute;
  left: 24px;
  top: 5px;
}
.timeline li .timeline-date {
  display: inline-block;
  width: 100%;
  color: #a6a6a6;
  font-style: italic;
  font-size: 13px;
}
.timeline.timeline-icons li {
  padding-top: 7px;
}
.timeline.timeline-icons li:after {
  width: 32px;
  height: 32px;
  background: #fff;
  border: 1px solid #e3e3e3;
  left: 14px;
  top: 0;
  z-index: 11;
}
.timeline.timeline-icons li .timeline-icon {
  position: absolute;
  left: 23.5px;
  top: 7px;
  z-index: 12;
}
.timeline.timeline-icons li .timeline-icon [class*=glyphicon] {
  top: -1px !important;
}
.timeline.timeline-icons.timeline-sm li {
  padding-left: 40px;
  margin-bottom: 10px;
}
.timeline.timeline-icons.timeline-sm li:after {
  left: -5px;
}
.timeline.timeline-icons.timeline-sm li .timeline-icon {
  left: 4.5px;
}
.timeline.timeline-advanced li {
  padding-top: 0;
}
.timeline.timeline-advanced li:after {
  background: #fff;
  border: 1px solid #29b6d8;
}
.timeline.timeline-advanced li:before {
  content: "";
  width: 52px;
  height: 52px;
  border: 10px solid #fff;
  position: absolute;
  left: 4px;
  top: -10px;
  border-radius: 50%;
  z-index: 12;
}
.timeline.timeline-advanced li .timeline-icon {
  color: #29b6d8;
}
.timeline.timeline-advanced li .timeline-date {
  width: 75px;
  position: absolute;
  right: 5px;
  top: 3px;
  text-align: right;
}
.timeline.timeline-advanced li .timeline-title {
  font-size: 12px;
  margin-bottom: 0;
  padding-top: 5px;
  font-weight: bold;
}
.timeline.timeline-advanced li .timeline-subtitle {
  display: inline-block;
  width: 100%;
  color: #a6a6a6;
}
.timeline.timeline-advanced li .timeline-content {
  margin-top: 10px;
  margin-bottom: 10px;
  padding-right: 70px;
}
.timeline.timeline-advanced li .timeline-content p {
  margin-bottom: 3px;
}
.timeline.timeline-advanced li .timeline-content .divider-dashed {
  padding-top: 0px;
  margin-bottom: 7px;
  width: 200px;
}
.timeline.timeline-advanced li .timeline-user {
  display: inline-block;
  width: 100%;
  margin-bottom: 10px;
}
.timeline.timeline-advanced li .timeline-user:before,
.timeline.timeline-advanced li .timeline-user:after {
  content: " ";
  display: table;
}
.timeline.timeline-advanced li .timeline-user:after {
  clear: both;
}
.timeline.timeline-advanced li .timeline-user .timeline-avatar {
  border-radius: 50%;
  width: 32px;
  height: 32px;
  float: left;
  margin-right: 10px;
}
.timeline.timeline-advanced li .timeline-user .timeline-user-name {
  font-weight: bold;
  margin-bottom: 0;
}
.timeline.timeline-advanced li .timeline-user .timeline-user-subtitle {
  color: #a6a6a6;
  margin-top: -4px;
  margin-bottom: 0;
}
.timeline.timeline-advanced li .timeline-link {
  margin-left: 5px;
  display: inline-block;
}
.timeline-load-more-btn {
  margin-left: 70px;
}
.timeline-load-more-btn i {
  margin-right: 5px;
}


/* -----------------------------------------
   Dropdown
----------------------------------------- */
.dropdown-menu{
    padding:0 0 0 0;
}
a.dropdown-menu-header {
    background: #328CCC;
    font-weight: bold;
   
    text-decoration:none;
    color:white;
}
.dropdown-menu > li a {
    padding: 10px 20px;
}

/* -----------------------------------------
   Badge
----------------------------------------- */

.scrollbox::-webkit-scrollbar-track
{
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
	border-radius: 10px;
	background-color: #F5F5F5;
}

.scrollbox::-webkit-scrollbar
{
	width: 12px;
	background-color: #F5F5F5;
}

.scrollbox::-webkit-scrollbar-thumb
{
	border-radius: 10px;
	-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
	background-color: #555;
}
background-color: #D62929;
}

	</style>
<div class="fixed-top"id="nav">
<nav class="navbar navbar-icon-top navbar-expand-lg navbar-light  shadow-sm rounded">
  <a class="navbar-brand" href="../../Anasayfa"style="; color:#328CCC;"><b><img src="../../logo.png"width="62px"height="55px"/></span></b></a>
    
 
    
    
    
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
     
     
     
      <div class="search-box float-left col-lg-10 "style="width:100%">
          
      <select class="form-control col-lg-3 col-md-2 "style="float:left" id="department"hidden>
         <?php
                     $department=$_COOKIE['userDepartmentId'];
                     $sql="SELECT *  FROM department WHERE id =  '$department'";
                     $result=mysqli_query($db,$sql);
                     while($row = mysqli_fetch_array($result)){
                     $faculty=$row['facultyId'];
                     }
                     ?>
                     <?php
                     $sql2="SELECT *  FROM department WHERE facultyId =  $faculty ORDER BY departmentName";
                     $result2=mysqli_query($db,$sql2); ?>
                     <?php
                     $sql3="SELECT *  FROM department WHERE id =  $department ";
                     $result3=mysqli_query($db,$sql3);
                     while($row3 = mysqli_fetch_array($result3)){
                     $userDepartment=$row3['departmentName'];
                     }?>
                     <option value="<?php echo $department; ?>"><?php echo $userDepartment; ?></option>
                    <?php while($row2= mysqli_fetch_array($result2)) { ?>
                        <option value="<?php echo $row2['id']; ?>">
                            <?php echo $row2['departmentName']; ?>
                        </option>
                    <?php } ?>
                    
      </select>
     <input class="form-control "style="" type="search" placeholder="Bir şeyler Ara.." aria-label="Search"  name="country" id="country"autocomplete="off" >
        <div class="result "style="width:100%"></div>
    </div>
     
     

         
            
 <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.search-box input[type="search"]').on("keyup input", function(){
        /* Get input value on change */
        var inputVal = $(this).val();
        var e = document.getElementById("department");
        var department = e.options[e.selectedIndex].value;
        var resultDropdown = $(this).siblings(".result");
        if(inputVal.length){
            $.get("../../search_data.php", {term: inputVal,department: department}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // Set search input value on click of result item
    $(document).on("click", ".result p", function(){
        $(this).parents(".search-box").find('input[type="search"]').val($(this).text());
        $(this).parent(".result").empty();
    });
});
</script>
    
    </ul>
   
   
   
   
   
   
   
   
    <ul class="navbar-nav ">
    <li class="nav-item">
        <a class="nav-link " href="../../NotEkle">
         	<i class="fas fa-lg fa-plus-circle"></i>
          Not Paylaş
        </a>
      </li>
     <li class="nav-item">
        <a class="nav-link " href="../../nOY">
         <i class="fas fa-lg fa-comments"></i>
          Tartışmalar
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled " href="../../GED">
          <i class="fas fa-lg fa-video"></i>
          (Yakında)
        </a>
      </li>
      <li class="dropdown nav-item" style="">
    <a href="#" onclick="return false;" data-toggle="dropdown" id="dropdownMenu1" data-target="#"   class="nav-link <?php if(!$_COOKIE['userDepartmentId']){echo "disabled";}?>">
         <i class="fas fa-lg fa-bell" style="  "> 
        </i> Bildirimler <span class="count badge  badge-info"></span> <i class="fas fa-caret-down"></i>
    </a>
    <ul class="dropdown-menu dropdown-menu-left pull-right " role="menu" aria-labelledby="dropdownMenu1">
       <li role="presentation" >
            <a class=" bg-transparent " style="color:black;float:left;text-decoration:none" >Bildirimler</a>
        </li>
       <li role="presentation" >
           <a href="#" class=" bg-transparent " style="float:right;color:black"><small><i class="fas fa-lg fa-cog"></i></small> </a>
        </li><br><br>
        <ul class="timeline timeline-icons timeline-sm scrollbox" style="margin:10px;width:210px">
                                       
                                        
        </ul>
         
        
    </ul>
</li>

            

         
        <script>
$(document).ready(function(){
 
 function load_unseen_notification(view = '')
 {
  $.ajax({
   url:"../../fetchnotification",
   method:"POST",
   data:{view:view},
   dataType:"json",
   success:function(data)
   {
    $('.timeline').html(data.notification);
    if(data.unseen_notification > 0)
    {
     $('.count').html(data.unseen_notification);
    }
   }
  });
 }
 
 load_unseen_notification();
 

 
 $(document).on('click', function(){
  $('.count').html('');
  load_unseen_notification('yes');
 });
 
 setInterval(function(){ 
  load_unseen_notification();; 
 }, 5000);
 
});
</script>
      <li class="nav-item">
        <a class="nav-link <?php if(!$_COOKIE['userDepartmentId']){echo "disabled";}?>" href="../../Kitaplık">
          	<i class="fas fa-lg fa-align-justify"></i>
          Kitaplık
        </a>
      </li>
     <?php if($_COOKIE['userDepartmentId']){?>
      <li class="nav-item">
       <div class="dropdown">
          <a class="nav-link " data-toggle="dropdown" href="#">
                  	<i class="fas fa-lg fa-user"></i>
                  Hesabım <i class="fas fa-caret-down"></i>
                </a>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="../../Kitaplık/Profilim"  style="color:#7F7F7F"><i class="fas  fa-user"></i>  Profilim</a>
            <a class="dropdown-item disabled"  style="color:#7F7F7F">	<i class="fas  fa-cog"></i> Ayarlar</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="../../logout.php" style="color:#7F7F7F"><i class="fas  fa-sign-out-alt"></i> Çıkış</a>
          </div>
        </div>
      </li>
      <?php } else{?>
      <li class="nav-item">
        <a href="../" class="btn btn-info ml-auto" >Giriş Yap</a> 
      </li>        
      <?php }?>
    </ul>
    
  </div>

</nav>
</div>
<script>
    $(function () {
    $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
    });
    
    $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
            $(this).removeClass('open');
        }
    });
    
    
    
</script>
         <script>
             $('.control').click( function(){
  $('body').addClass('search-active');
  $('.input-search').focus();
});

$('.icon-close').click( function(){
  $('body').removeClass('search-active');
});
         </script>