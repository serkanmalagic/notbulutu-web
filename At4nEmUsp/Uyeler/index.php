        <?php $ROOT="../"; include("../header.php"); ?>
        <?php include("../navbar.php"); ?>
        <?php include("../../connect.php");
        if(isset($_GET['filter'])){
                $filter = $_GET['filter'];
            }
            else{
            $filter ="created_at";
            }?>
        <?php 
            date_default_timezone_set('Europe/Istanbul');
    
            function paylasilma_zamani ($time)
            {
            
                $time = time() - $time; // to get the time since that moment
                $time = ($time<1)? 1 : $time;
                $tokens = array (
                    31536000 => 'yıl',
                    2592000 => 'ay',
                    604800 => 'hafta',
                    86400 => 'gün',
                    3600 => 'saat',
                    60 => 'dakika',
                    1 => 'saniye'
                );
            
                foreach ($tokens as $unit => $text) {
                    if ($time < $unit) continue;
                    $numberOfUnits = floor($time / $unit);
                    return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
                }
            
              }?>
              <?php 
                                                        $sql1="Select * from user";
                                                        $result1=mysqli_query($db,$sql1);
                                                        $count1 = mysqli_num_rows($result1);
                                                        
                                                        ?>
        <body>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    
                        <div class="row">
                            <!-- ============================================================== -->
                      
                            <!-- ============================================================== -->

                                          <!-- recent orders  -->
                            <!-- ============================================================== -->
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">Üye İşlemleri</h5>
                                    <div class="card-body p-0">
                                        <div class="table-responsive">
                                            <br>
                                            <input type="search" class="light-table-filter form-control" data-table="order-table" placeholder="Filtrele">
                                            <br>
                                            <div style="margin-left:1%">
                                           <h4 style="color:white"> Sıralama Ölçütü</h4>
                                           
                                            <a href="./?filter=name" class="btn btn-outline-success" title="İsime Göre Sırala"><i class="fas fa-user"></i></a>
                                            <a href="./?filter=departmentId" class="btn btn-outline-danger" title="Bölüme Göre Sırala"><i class="fas fa-building"></i></a>
                                            <a href="./?filter=created_at" class="btn btn-outline-info"title="Katılma Tarihine Göre Sırala"><i class="fas fa-clock"></i></a>
                                            <span style="float:right;margin-right:2%;color:white;font-size:25px"><?php echo $count1;?> Üye</span>
                                            </div>
                                            <br>
                                            <table class="table table-striped order-table table" id="mytable">
                                                <thead class="bg-light">
                                                    <tr class="border-0">
                                                        <th class="border-0">Id</th>
                                                        <th class="border-0">Adı</th>
                                                        <th class="border-0">Bölüm Id</th>
                                                        <th class="border-0">Mail</th>
                                                        <th class="border-0">Okul Numarası</th>
                                                        <th class="border-0">Rolü</th>
                                                        <th class="border-0">Durum</th>
                                                        <th class="border-0">Kayıt Tarihi</th>
                                                        <th class="border-0">Güncelle</th>
                                                        <th class="border-0">Sil</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sql="Select * from user ORDER BY $filter DESC";
                                                        $result=mysqli_query($db,$sql);
                                                        $count = mysqli_num_rows($result);
                                                        while($row= mysqli_fetch_array($result)){
                                                            $time = strtotime($row['created_at']);
                                                        ?>
                                                    <tr>
                                                        <td><?php echo $row['id']; ?> </td>
                                                        <td><?php echo $row['name']; ?></td>
                                                        <?php $departmentId =  $row['departmentId'];
                                                                $sqlDepartment ="Select * from department where id = $departmentId";
                                                                $resultDepartment=mysqli_query($db,$sqlDepartment);
                                                                while($rowDepartment= mysqli_fetch_array($resultDepartment)){
                                                                    $department =$rowDepartment['departmentName'];
                                                                }
                                                                ?>
                                                        
                                                        <td><?php echo $department." (".$row['departmentId'].")"; ?></td>
                                                        <td><?php echo $row['mail']; ?></td>
                                                        <td><?php echo $row['schoolnumber']; ?></td>
                                                        <td><?php echo $row['role']; ?></td>
                                                        <td><?php if($row['status']=="true"){ ?><span class="badge-dot badge-success badge-brand mr-1"></span><?php }else{ ?><span class="badge-dot  badge-brand mr-1"></span><?php } echo $row['status']; ?></td>
                                                        <td><?php echo paylasilma_zamani($time).' önce'; ?></td>
                                                        <td>
                                                        
                                                            
                                                            <button class="btn btn-sm btn-warning button-block"data-toggle="modal" data-target="#guncelle<?php echo $row['id']; ?>"><i class="far fa-edit"></i> Güncelle</button>
                                                        
                                                        </td>
                                                        <td><button class="btn btn-sm btn-outline-danger button-block"data-toggle="modal" data-target="#modal<?php echo $row['id']; ?>"><i class="far fa-trash-alt"></i> Sil</button></td>
                                                        
                                                    </tr>
                                                    <?php include("update.php");?>
                                                    
                                                    <!-- SiL Modal -->
                                                    
                                                    <div class="modal fade" id="modal<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">#<?php echo $row['id']; ?> <?php echo $row['lectureName']; ?> başlıklı notu silmek üzeresiniz</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-footer">
                                                            <form action="../delete" method="post">
                                                                <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                                <input type="hidden" name="type" value="user">
                                                                <button type="submit" class="btn btn-danger btn-block">Sil</button>
                                                            </form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    
                                                    
                                                    <?php } ?>
                                                    
                                                </tbody>
                                            </table>
                                             <!--Filtrele-->
                                            <script>
                                                (function(document) {
                                                	'use strict';
                                                
                                                	var LightTableFilter = (function(Arr) {
                                                
                                                		var _input;
                                                
                                                		function _onInputEvent(e) {
                                                			_input = e.target;
                                                			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
                                                			Arr.forEach.call(tables, function(table) {
                                                				Arr.forEach.call(table.tBodies, function(tbody) {
                                                					Arr.forEach.call(tbody.rows, _filter);
                                                				});
                                                			});
                                                		}
                                                
                                                		function _filter(row) {
                                                			var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
                                                			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
                                                		}
                                                
                                                		return {
                                                			init: function() {
                                                				var inputs = document.getElementsByClassName('light-table-filter');
                                                				Arr.forEach.call(inputs, function(input) {
                                                					input.oninput = _onInputEvent;
                                                				});
                                                			}
                                                		};
                                                	})(Array.prototype);
                                                
                                                	document.addEventListener('readystatechange', function() {
                                                		if (document.readyState === 'complete') {
                                                			LightTableFilter.init();
                                                		}
                                                	});
                                                
                                                })(document);
                                                </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end recent orders  -->

    
                            
           
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <!-- Modal -->
    
    
    
    <script src="../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="../assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="../assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="../assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="../assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="../assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="../assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="../assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="../assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="../assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>