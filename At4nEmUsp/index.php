<!doctype html>
<html lang="en">
 <?php
    include ("../header.php");
 ?>
<link rel="canonical" href="https://getbootstrap.com/docs/3.4/examples/signin/">
  <body>
    <div class="col-lg-4"></div>
    <div class="container col-lg-4" style= "margin-top: 5%;">

      <form class="form-signin"action="login"method="post">
          <img src="../../logo.png"width="72px"height="65px"/>
        <h2 class="form-signin-heading">Admin Giriş Paneli</h2>
        <label for="inputEmail" class="sr-only">Email adresi</label><br>
        <input type="text" id="inputEmail" name="user_name" class="form-control" placeholder="Kullanıcı Adı" required autofocus>
        <label for="inputPassword" class="sr-only">Şifre</label><br>
        <input type="password" id="inputPassword" name="user_password" class="form-control" placeholder="Şifre" required><br>
        <br>
        <button class="btn btn-lg btn-outline-danger btn-block" type="submit">Giriş Yap</button>
      </form>

    </div> <!-- /container -->
<div class="col-lg-4"></div>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>