        <?php $ROOT="../"; include("../header.php"); ?>
        <?php include("../navbar.php"); ?>
        <?php include("../../connect.php"); 
            if(isset($_GET['filter'])){
                $filter = $_GET['filter'];
            }
            else{
            $filter ="created_at";
            }
            ?>
        <?php 
            date_default_timezone_set('Europe/Istanbul');
    
            function paylasilma_zamani ($time)
            {
            
                $time = time() - $time; // to get the time since that moment
                $time = ($time<1)? 1 : $time;
                $tokens = array (
                    31536000 => 'yıl',
                    2592000 => 'ay',
                    604800 => 'hafta',
                    86400 => 'gün',
                    3600 => 'saat',
                    60 => 'dakika',
                    1 => 'saniye'
                );
            
                foreach ($tokens as $unit => $text) {
                    if ($time < $unit) continue;
                    $numberOfUnits = floor($time / $unit);
                    return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
                }
            
              }?>
        <body>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    
                        <div class="row">
                            <!-- ============================================================== -->
                      
                            <!-- ============================================================== -->

                                          <!-- recent orders  -->
                            <!-- ============================================================== -->
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">Ders Notu işlemleri</h5>
                                    <div class="card-body p-0">
                                        <div class="table-responsive table-striped">
                                            <br>
                                           <input type="search" class="light-table-filter form-control" data-table="order-table" placeholder="Filtrele">
                                           <br>
                                           
	                                        <table class="order-table table">
                                                
                                                <thead class="bg-light">
                                                    <tr class="border-0">
                                                        <th class="border-0">Id</th>
                                                        <th class="border-0">Ders Adı</th>
                                                        <th class="border-0">Açıklama</th>
                                                        <th class="border-0"><a href="./?filter=noteView" style="color:white" > Görüntülenme</a></th>
                                                        <th class="border-0"><a href="./?filter=created_at" style="color:white" > Yayın Tarihi</a></th>
                                                        <th class="border-0">  <a href="./?filter=noteLike" style="color:white" >Beğeni</a></th>
                                                        <th class="border-0">Durumu</th>
                                                        <th class="border-0">Sil</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sql="Select * from note ORDER BY $filter DESC";
                                                        $result=mysqli_query($db,$sql);
                                                        while($row= mysqli_fetch_array($result)){
                                                            $time = strtotime($row['created_at']);
                                                        ?>
                                                    <tr>
                                                        <td><?php echo $row['id']; ?> </td>
                                                        <td><?php echo $row['className']; ?></td>
                                                        <td><?php echo $row['description']; ?></td>
                                                        <td style="text-align:center"><?php echo $row['noteView']; ?></td>
                                                        <td><?php echo paylasilma_zamani($time).' önce'; ?></td>
                                                        <td style="text-align:center"><?php echo $row['noteLike']; ?></td>
                                                        <td><span class="badge-dot badge-brand mr-1"></span><?php echo $row['status']; ?> </td>
                                                        <td><button class="btn btn-sm btn-outline-danger button-block"data-toggle="modal" data-target="#modal<?php echo $row['id']; ?>"><i class="far fa-trash-alt"></i> Sil</button></td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    
                                                    <div class="modal fade" id="modal<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">#<?php echo $row['id']; ?> <?php echo $row['className']; ?> başlıklı notu silmek üzeresiniz</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-footer">
                                                            <form action="../delete" method="post">
                                                                <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                                <input type="hidden" name="type" value="note">
                                                                <button type="submit" class="btn btn-danger btn-block">Sil</button>
                                                            </form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    
                                                    <?php } ?>
                                                    <tr>
                                                        <td colspan="9"><a href="#" class="btn btn-outline-light float-right">View Details</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!--Filtrele-->
                                            <script>
                                                (function(document) {
                                                	'use strict';
                                                
                                                	var LightTableFilter = (function(Arr) {
                                                
                                                		var _input;
                                                
                                                		function _onInputEvent(e) {
                                                			_input = e.target;
                                                			var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
                                                			Arr.forEach.call(tables, function(table) {
                                                				Arr.forEach.call(table.tBodies, function(tbody) {
                                                					Arr.forEach.call(tbody.rows, _filter);
                                                				});
                                                			});
                                                		}
                                                
                                                		function _filter(row) {
                                                			var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
                                                			row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
                                                		}
                                                
                                                		return {
                                                			init: function() {
                                                				var inputs = document.getElementsByClassName('light-table-filter');
                                                				Arr.forEach.call(inputs, function(input) {
                                                					input.oninput = _onInputEvent;
                                                				});
                                                			}
                                                		};
                                                	})(Array.prototype);
                                                
                                                	document.addEventListener('readystatechange', function() {
                                                		if (document.readyState === 'complete') {
                                                			LightTableFilter.init();
                                                		}
                                                	});
                                                
                                                })(document);
                                                </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end recent orders  -->

    
                            
           
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <!-- Modal -->
    
    
    
    <script src="../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="../assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="../assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="../assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="../assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="../assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="../assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="../assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="../assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="../assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>