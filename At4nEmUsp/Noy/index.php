        <?php $ROOT="../"; include("../header.php"); ?>
        <?php include("../navbar.php"); ?>
        <?php include("../../connect.php"); ?>
        <?php 
            date_default_timezone_set('Europe/Istanbul');
    
            function paylasilma_zamani ($time)
            {
            
                $time = time() - $time; // to get the time since that moment
                $time = ($time<1)? 1 : $time;
                $tokens = array (
                    31536000 => 'yıl',
                    2592000 => 'ay',
                    604800 => 'hafta',
                    86400 => 'gün',
                    3600 => 'saat',
                    60 => 'dakika',
                    1 => 'saniye'
                );
            
                foreach ($tokens as $unit => $text) {
                    if ($time < $unit) continue;
                    $numberOfUnits = floor($time / $unit);
                    return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
                }
            
              }?>
        <body>
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    
                        <div class="row">
                            <!-- ============================================================== -->
                      
                            <!-- ============================================================== -->

                                          <!-- recent orders  -->
                            <!-- ============================================================== -->
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">NOY işlemleri</h5>
                                    <div class="card-body p-0">
                                        <div class="table-responsive">
                                            <br>
                                            <input class="form-control" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names..">
                                            <br>
                                            <table class="table table-striped" id="mytable">
                                                <thead class="bg-light">
                                                    <tr class="border-0">
                                                        <th class="border-0">Id</th>
                                                        <th class="border-0">Başlık</th>
                                                        <th class="border-0">Açıklama</th>
                                                        <th class="border-0">Katılım Oranı</th>
                                                        <th class="border-0">Yayın Tarihi</th>
                                                        <th class="border-0">Durumu</th>
                                                        <th class="border-0">Sil</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php 
                                                        $sql="Select * from noy ORDER BY created_at DESC";
                                                        $result=mysqli_query($db,$sql);
                                                        while($row= mysqli_fetch_array($result)){
                                                            $time = strtotime($row['created_at']);
                                                        ?>
                                                    <tr>
                                                        <td><?php echo $row['id']; ?> </td>
                                                        <td><?php echo $row['title']; ?></td>
                                                        <td><?php echo $row['description']; ?></td>
                                                        <td><?php echo "%".round(($row['noyCouny']/$row['goal'])*100); ?></td>
                                                        <td><?php echo paylasilma_zamani($time).' önce'; ?></td>
                                                        <td><?php if($row['status']=="true"){ ?><span class="badge-dot badge-success badge-brand mr-1"></span><?php }else{ ?><span class="badge-dot  badge-brand mr-1"></span><?php } echo $row['status']; ?></td>
                                                        <td><button class="btn btn-sm btn-outline-danger button-block"data-toggle="modal" data-target="#modal<?php echo $row['id']; ?>"><i class="far fa-trash-alt"></i> Sil</button></td>
                                                    </tr>
                                                    <!-- Modal -->
                                                    
                                                    <div class="modal fade" id="modal<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">#<?php echo $row['id']; ?> <?php echo $row['lectureName']; ?> başlıklı notu silmek üzeresiniz</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                              <span aria-hidden="true">&times;</span>
                                                            </button>
                                                          </div>
                                                          <div class="modal-footer">
                                                            <form action="../delete" method="post">
                                                                <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                                                                <input type="hidden" name="type" value="noy">
                                                                <button type="submit" class="btn btn-danger btn-block">Sil</button>
                                                            </form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    
                                                    <?php } ?>
                                                    
                                                </tbody>
                                            </table>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end recent orders  -->

    
                            
           
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <!-- Modal -->
    
    
    
    <script src="../assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="../assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="../assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="../assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="../assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="../assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="../assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="../assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="../assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="../assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="../assets/libs/js/dashboard-ecommerce.js"></script>
</body>
 
</html>