<?php

    date_default_timezone_set('Europe/Istanbul');

function paylasilma_zamani ($time)
{

    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'yıl',
        2592000 => 'ay',
        604800 => 'hafta',
        86400 => 'gün',
        3600 => 'saat',
        60 => 'dakika',
        1 => 'saniye'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
    }

}


?>
<?php


	if (isset($_POST['getData'])) {
		$conn = new mysqli('localhost', 'admin', 'parola098', 'notbulutu');

		$start = $conn->real_escape_string($_POST['start']);
		$limit = $conn->real_escape_string($_POST['limit']);
        $number=$_COOKIE['userDepartmentId'];
        if($number){
    		$sql = $conn->query("SELECT n.*, u.name, u.departmentId, u.schoolnumber, u.id as userId, u.profilePictureUrl as profilePictureUrl
            FROM note n  
            LEFT JOIN user u ON n.userId = u.id WHERE n.did='$number' and n.status='true' ORDER BY id DESC LIMIT $start, $limit");
        }
        else{
            $sql= $conn->query("SELECT n.*, u.name, u.departmentId, u.schoolnumber, u.id as userId, u.profilePictureUrl as profilePictureUrl
             FROM note n  
            LEFT JOIN user u ON n.userId = u.id WHERE n.status='true'  ORDER BY id DESC LIMIT $start, $limit");
        }
		if ($sql->num_rows > 0) {
			$response = "";

			while($row= $sql->fetch_array()) {
                 $noteId=$row['id'];
                 $classId=$row['title'];
                 $noteHit=$row['noteLike'];
                 $userId=$row['userId'];

                 $sqlImage= $conn->query("SELECT * FROM image WHERE noteId = '$noteId' ORDER BY id  LIMIT 1 ");
                
                 while($rowImage = $sqlImage->fetch_array()){ 
                     $time = strtotime($row['created_at']);

                      $sqlClass=$conn->query("SELECT * FROM `class` WHERE id = '$classId' ORDER BY id DESC  LIMIT 1 ");
                      
                      while($rowClass=$sqlClass->fetch_array()){
                           if($rowImage['type']=="image/jpeg" ){
  
$response .= '
                <div class="card gedf-card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="mr-2">
                                    <img class="rounded-circle" width="45" src="../Kitaplık/Profilim/uploads/'.$row['profilePictureUrl'].'" alt="">
                                </div>
                                <div class="ml-2">
                                    <div class="h6 m-0"><a style="text-decoration:none"href="../Profil?id='. $row['userId'].'">'. $row['name'].'</a> </div>
                                     <div class="text-muted h7 mb-2"> <i class="far fa-sm fa-clock"></i> '.paylasilma_zamani($time).' önce</div>
                                </div>
                            </div>
                            <div>
                                <div class="dropdown">
                                    <button class="btn btn-link dropdown-toggle" type="button" id="gedf-drop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop1">
                                        <div class="h6 dropdown-header">Seçenekler</div>
                                        <!--<a class="dropdown-item" href="">Kitaplığa Ekle</a>-->
                                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#report'. $rowClass['id'].'">Raporla</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                                                <!-- Modal -->
                        <div class="modal fade" id="report'. $rowClass['id'].'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Sorunu anlamamıza yardımcı ol</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <form action="?report=success" method="post">
                                <div class="radio">
                                  <label><input type="radio" name="optradio" checked> Uygunsuz İçerik</label><br>
                                   <label><input type="radio" name="optradio" checked> Spam</label>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Raporu Gönder</button>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        
                    </div>
                    <a href="notDetay.php?id='.$noteId.'">  <img class="card-img-top "style="" src="../images/'. $rowImage['name'].'"alt="Card image" style="width:100%"></a> 
                    <div class="card-body">
                       
                        <a class="card-link" href="#">
                            <h5 class="card-title">'. $rowClass['className'].'</h5>
                        </a>

                        <p class="card-text">
                            '. $row['description'].'
                        </p>
                    </div>
                    <div class="card-footer">
                        <a href="notDetay.php?id='. $noteId .'" class="card-link"><i class="fa fa-comment"></i> Yorumlar</a>
                        <a href="notDetay.php?id='. $noteId .'" class="card-link"><i class="fab fa-gratipay"></i></i>  '. $row['noteLike'].' Beğeni</a>    
                        <a  class="card-link"style="color:#007BFF"><i class="fas fa-eye"></i> '. $row['noteView'].' Görüntülenme</a>
                       <!-- <a href="notDetay.php?id=<?php echo $noteId ?>" class="card-link" ><i class="fas fa-share"></i> Paylaş</a>-->
                        
                        
                        
                    </div>
                </div>
                <br>
                
               ';
                          
				
			}}}}

			exit($response);
		} else
			exit('reachedMax');
	}
?>