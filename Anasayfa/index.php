<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Akış</title>
        <!-- Latest compiled and minified CSS -->
        <?php include('../header.php');?>
        
    </head>
    <body>
        <style>
  .a{
      color:#328CCC;
  }
  .h7 {
            font-size: 0.8rem;
        }

        .gedf-wrapper {
            margin-top: 0.97rem;
        }

        @media (min-width: 992px) {
            .gedf-main {
                padding-left: 4rem;
                padding-right: 4rem;
            }
            .gedf-card {
                margin-bottom: 2.77rem;
            }
             .a{
      color:#328CCC;
  }
        }

        /**Reset Bootstrap*/
        .dropdown-toggle::after {s
            content: none;
            display: none;
        }
        
         #loader{
             height:50px;
             width:50px;
            display: block;
            margin: auto;
            background-color:transparent;
        }
        </style>
        <?php include('../navbar.php');?>
        <div class="container" id="content" style="margin-top: 90px">
            <div class="row">
                <div class="col-lg-3 hidden-sm hidden-xs  "style="border:1px ;">
                </div>
                <div class="col-lg-6 results"id="response">
                  
                </div>
                <div class="col-lg-3 hidden-sm hidden-xs  "style="border:1px ;">
                </div>
            </div>
            <center> <span id="end" style="display:none">Not Bulunamadı</span><img src="loading.gif" id="loader" width="50px"heigth="50px"style="float:center"/></center><br><br>
        </div>

       
        
        <script type="text/javascript">
            var start = 0;
            var limit = 5;
            var reachedMax = false;
           
           $(window).on("scroll", function() {
            var docHeight = $(document).height();
            var winScrolled = $(window).height() + $(window).scrollTop(); // Sum never quite reaches
            if ((docHeight - winScrolled) < 1) {
                getData();
            }
        });

            $(document).ready(function () {
               getData();
            });

            function getData() {
                if (reachedMax)
                    return;

                $.ajax({
                   url: 'data',
                   method: 'POST',
                    dataType: 'text',
                   data: {
                       getData: 1,
                       start: start,
                       limit: limit
                   },
                   success: function(response) {
                        if (response == "reachedMax"){
                            reachedMax = true;
                            document.getElementById("loader").style.display = "none";
                             document.getElementById("end").style.display = "inline";
                        }
                        else {
                            start += limit;
                            $(".results").append(response);
                        }
                    }
                });
            }
        </script>
        <button onclick="topFunction()" id="myBtn" title="Yukarı çık"><i class="fas fa-chevron-circle-up"></i></button>
<script>// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}</script>
        
<script>
$( "body" ).prepend( '<div id="preloader" ><div class="spinner" id="status"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>' );
$(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#preloader').delay(1650).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(1650).css({'overflow':'visible'});
})</script>
    </body>
</html>