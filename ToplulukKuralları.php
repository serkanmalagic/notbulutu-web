<?php include('header.php');?>
<?php include('connect.php')?>
<title>Topluluk Kuralları</title>
<body>

    <?php include('navbar.php')?>
    <div class="col-lg-3"></div>
<div class="container col-lg-6"id="content"style="margin-top:90">
    	
   
    		<div class="card" style="width: 100%;">
      
      <div class="card-body">
        <h5 class="card-title">Topluluk Kurallarımız</h5>
        <p class="card-text">Bu sistem öğrencilerin yasal sınırlar çerçevesinde fikirlerini ve isteklerini dile getirmeleri amacıyla kurulmuştur. <br>
- Bağlı bulunduğunuz üniversite ile alakalı yapıcı olmak şartıyla eleştiri yapabilirsiniz.<br>
- Eğer yasadışı NOY açarsanız sistemden atılacaksınız. <br>

- Üniversite her türlü bilgiyi tarafımıza arz edebilir. <br>

- Karşılaştığınız sıkıntıları, sorunları dile getirin. Yeterli çoğunluğa ulaştığında ilgili mecralara iletin. <br>

- Oluşturduğunuz fikrin birinci sorumlusunun kendiniz olduğunu unutmayın.<br>

 <h5 class="card-title">Neden böyle bir şey var ?</h5>
 Öğrencilerin hepsinin okudukları üniversite hakkında bir fikirleri vardır. Fakat bazen bu fikirleri üst sıralara taşımak zor olabiliyor. Birlikten kuvvet doğar sözüyle fikir topluluğu oluşturan öğrenciler bulundukları yerin sorunlarına çözüm sağlanmasına yardımcı olacaklar.</p>
       <span style="color:#328CCC">notbulutu</span> Yönetim Ekibi.
      </div>
    </div>
</div>
<div class="col-lg-3"></div>