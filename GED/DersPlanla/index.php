<header>
    <?php session_start(); include('../../connect.php'); ?>
<meta charset="utf-8">
<title>GED</title>
<link rel="icon" href="logo.ico">
      <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
	
@import url(http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700);
body {
    margin:40px;
    font-family: 'Roboto Condensed', sans-serif;
    background-color: #EEEEEE;
    
}

	a:hover{
		text-decoration:none;
	}

.head {
    color: blue;
    margin-bottom: 40px;
}

.container  {
    background-color: #EEEEEE;
    padding: 30px;
}

.stepwizard-step p {
    margin-top: 10px;   
}

.stepwizard-row {
    display: table-row;
}

.stepwizard {
    display: table;     
    width: 100%;
    position: relative;
}

.stepwizard-step button[disabled] {
    background: #888;
    opacity: 1 !important;
    filter: alpha(opacity=100) !important;
}

.stepwizard-row:before {
    top: 50px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 100%;
    height: 3px;
    background-color: #BDC3C7;
    z-order: 0;
    
}

.stepwizard-step {    
    display: table-cell;
    text-align: center;
    position: relative;
}

.stepwizard-step p {
    margin-top:10px;
    
}



.btn-success .glyphicon {
    color: #fff;
    
    font-size: 40px;
}

.btn-circle {
  width: 100px;
  height: 100px;
  text-align: center;
  padding: 25px 25px;
  font-size: 40px;
  line-height: 1.428571429;
  border-radius: 50px;
  
  
}

/* 
// Listrap v1.0, by Gustavo Gondim (http://github.com/ggondim)
// Licenced under MIT License
// For updates, improvements and issues, see https://github.com/inosoftbr/listrap
*/

.listrap {
            list-style-type: none;
            margin: 0;
            padding: 0;
            cursor: default;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .listrap li {
            margin: 0;
            padding: 10px;
        }

        .listrap li.active, .listrap li:hover {
            background-color: #d9edf7;
        }

        .listrap strong {
            margin-left: 10px;
        }

        .listrap .listrap-toggle {
            display: inline-block;
            width: 60px;
            height: 60px;
        }

        .listrap .listrap-toggle span {
            background-color: #428bca;
            opacity: 0.8;
            z-index: 100;
            width: 60px;
            height: 60px;
            display: none;
            position: absolute;
            border-radius: 50%;
            text-align: center;
            line-height: 60px;
            vertical-align: middle;
            color: #ffffff;
        }

        .listrap .listrap-toggle span:before {
            font-family: 'Glyphicons Halflings';
            content: "\e013";
        }

        .listrap li.active .listrap-toggle span {
            display: block;
        }
}



.dropdown.dropdown-lg .dropdown-menu {
    margin-top: -1px;
    padding: 6px 20px;
}
.input-group-btn .btn-group {
    display: flex !important;
}
.btn-group .btn {
    border-radius: 0;
    margin-left: -1px;
}
.btn-group .btn:last-child {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
    
}
.btn-group .form-horizontal .btn[type="submit"] {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.form-horizontal .form-group {
    margin-left: 0;
    margin-right: 0;
}
.form-group .form-control:last-child {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
}



@media screen and (min-width: 768px) {
    #adv-search {
        width: 500px;
        
    }
    .dropdown.dropdown-lg {
        position: static !important;
    }
    .dropdown.dropdown-lg .dropdown-menu {
        min-width: 500px;
    }
}

.panel-pricing {
  -moz-transition: all .3s ease;
  -o-transition: all .3s ease;
  -webkit-transition: all .3s ease;
}
.panel-pricing:hover {
  box-shadow: 0px 0px 30px rgba(0, 0, 0, 0.7);
}
.panel-pricing .panel-heading {
  padding: 20px 10px;
}
.panel-pricing .panel-heading .fa {
  margin-top: 10px;
  font-size: 58px;
}
.panel-pricing .list-group-item {
  color: #777777;
  border-bottom: 1px solid rgba(250, 250, 250, 0.5);
}
.panel-pricing .list-group-item:last-child {
  border-bottom-right-radius: 0px;
  border-bottom-left-radius: 0px;
}
.panel-pricing .list-group-item:first-child {
  border-top-right-radius: 0px;
  border-top-left-radius: 0px;
}
.panel-pricing .panel-body {
  background-color: #f0f0f0;
  font-size: 40px;
  color: #777777;
  padding: 20px;
  margin: 0px;
}
	</style>
	<script>
	$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-info').addClass('btn-default');
            $item.addClass('btn-info');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-info').trigger('click');
});

// Listrap v1.0, by Gustavo Gondim (http://github.com/ggondim)
// Licenced under MIT License
// For updates, improvements and issues, see https://github.com/inosoftbr/listrap

jQuery.fn.extend({
    listrap: function () {
        var listrap = this;
        listrap.getSelection = function () {
            var selection = new Array();
            listrap.children("li.active").each(function (ix, el) {
                selection.push($(el)[0]);
            });
            return selection;
        }
        var toggle = "li .listrap-toggle ";
        var selectionChanged = function() {
            $(this).parent().parent().toggleClass("active");
            listrap.trigger("selection-changed", [listrap.getSelection()]);
        }
        $(listrap).find(toggle + "img").on("click", selectionChanged);
        $(listrap).find(toggle + "span").on("click", selectionChanged);
        return listrap;
    }
});
$(document).ready(function () {
    $(".listrap").listrap().on("selection-changed", function (event, selection) {
        console.log(selection);
    });
});</script>
</header>
<body>
<div class="head"style="">
    <a href="../../Anasayfa" style="color: #328CCC;font-size:35px"title="Anasayfa"> <b><i class="glyphicon glyphicon-cloud"></i>  </b></a> <span style="color: #328CCC;font-size:35px;"> Ders<b>Planla</b></span>
</div>



<div class="container" style="background-color: whitesmoke;border-top-right-radius: 10px;border-top-left-radius: 10px;">
    <div class="stepwizard ">
        <div class="stepwizard-row setup-panel row">
            <div class="stepwizard-step col-lg-3 col-xs-3">
                <a href="#step-1" type="button" class="btn btn-info btn-circle">
                <i class="glyphicon glyphicon-user"></i></a>    
            </div>
            <div class="stepwizard-step col-lg-3 col-xs-3">
                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">
                <i class="glyphicon glyphicon-calendar"></i></a>
            </div>
            <div class="stepwizard-step col-lg-3 col-xs-3">
                <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">
                <i class="glyphicon glyphicon-book"></i></a> 
            </div>
            <div class="stepwizard-step col-lg-3 col-xs-3">
                <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">
                <i class="glyphicon glyphicon-ok"></i></a>  
            </div>
        </div>
    </div>
<form action="meetup" method="post">
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12">
                <h3>Tarih, Saat & Yer</h3>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Tarih *</label>
                            <input  maxlength="100" id="date" name="date" type="date" required="required" class="form-control" placeholder=""  />
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Saat *</label>
                            <input id="time" name="time" maxlength="100" type="time" required="required" class="form-control" placeholder=""  />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Yer *</label>
                            <textarea  id="yer" name="yer"  class="form-control" required="required"  > </textarea>
                        </div>
                    </div>
                 
                   
                </div>
                
                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Devam</button> 
            </div>
        </div>
    </div>

<div class="row setup-content" id="step-1">
    <div class="col-xs-12">
        <div class="col-md-12">
            <div class="container "style="background-color: whitesmoke">
               
                <h3>Kişisel Bilgiler</h3><br>
                
                
                <div class="row">
                        <div class="col-md-12">
                        
                            <div class="input-group" id="adv-search">
                               <div class="form-group">
								<label class="control-label">Ad Soyad</label>
								<input  type="text" id="ad" name="ad" class="form-control" required="required" value="<?php echo $_COOKIE['userName'];?>" disabled/>
								</div><br><br>
                                <div class="form-group">
								<!--<label class="control-label">Telefon (opsiyonel)</label>
								<input  type="tel" id="tel" name="tel"  class="form-control" required="required"  />-->
								
								</div>
						  <a href="#" data-toggle="popover" title="İsteğe bağlı" data-content="Katılımcıların sizinle daha hızlı ve kolay iletişime geçebilmesi için.">Neden telefon numaramı yazmalıyım?</a>


<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
});
</script>
                           
                         
                           
                            </div>
                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Devam</button>
                        </div>
                </div>
	               
                
            
                </div>
            </div>
        </div>
    </div> 
</div>

<div class="row setup-content" id="step-3">
    <div class="col-xs-12">
        <div class="col-md-12">
            <div class="container"style="background-color: whitesmoke">
            <h3>Ders Detayları</h3>
                </br>
                 <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Dersin Adı *</label>
                            <select class=" form-control" id="dad" name="dad">
                               
                                <?php
                                $department = $_COOKIE["userDepartmentId"];
                                 $sql="SELECT *  FROM class WHERE did =  $department ORDER BY className";
                                 $result=mysqli_query($db,$sql); ?>
                                <?php while($row= mysqli_fetch_array($result)) { ?>
                                    <option value="<?php echo $row['className']; ?>">
                                        <?php echo $row['className']; ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Dersin Konusu *</label>
                            <input  maxlength="100" id="konu" name="konu" type="text" required="required" class="form-control" placeholder=""  />
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Tahmini eğitim süresi (saat) *</label>
                            <input  maxlength="100" id="sure" name="sure" type="number" required="required" class="form-control" placeholder=""  />
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Katılabilecek kişi sayısı (max) *</label>
                            <input  maxlength="100" id="kisi" name="kisi" type="number" required="required" class="form-control" placeholder=""  />
                        </div>
                    </div>
                 <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Devam</button>
            </div>
        </div> 
    </div>
</div>
    <div class="row setup-content" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12">
                <div class="container"style="background-color: whitesmoke">
                
                      <button type="button" class="btn btn-block btn-info" data-toggle="modal" data-target="#exampleModal"onclick="myFunction()"><h2>Özet Görüntüle</h2>
</button>
                       <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title" id="exampleModalLabel">Bilgilerini gözden geçir</h1>
      
      </div>
      <div class="modal-body">
        <p id="mytext"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kapat</button>
        <button type="submit" class="btn btn-lg btn-success completeBtn">Tamamla</button>
      </div>
    </div>
  </div>
</div>
					
                   <script>
					 function myFunction() {
					   var ad = document.getElementById("ad").value;
						/*var tel = document.getElementById("tel").value;*/
						 var date = document.getElementById("date").value;
						  var time = document.getElementById("time").value;
						  var yer = document.getElementById("yer").value;
						  var dad = document.getElementById("dad").value;
						  var konu = document.getElementById("konu").value;
						  var sure = document.getElementById("sure").value;
						  var kisi = document.getElementById("kisi").value;
								document.getElementById("mytext").innerHTML ="<h3>Ad Soyad :<b> "+ ad +"</b></h3>"/*+"<h3>Telefon : <b> "+ tel +"</b></h3>"*/+"<h3>Tarih : <b>"+ date +"</b></h3>"+"<h3>Saat :<b> "+ time +"</b></h3>"+"<h3>Yer : <b>"+ yer +"</b></h3>"+"<h3>Dersin Adı :<b> "+ dad +"</b></h3>"+"<h3>Dersin Konusu :<b> "+ konu +"</b></h3>"+"<h3>Süre :<b> "+ sure +"</b></h3>"+"<h3>Kişi Sayısı :<b> "+ kisi +"</b></h3>";
					 }
					</script> 
               <!--<button class="btn btn-success completeBtn btn-lg pull-right" type="submit" >Tamamla</button>-->
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
