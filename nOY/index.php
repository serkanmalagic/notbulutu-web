<?php  date_default_timezone_set('Europe/Istanbul');

function paylasilma_zamani ($time)
{

    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'y.',
        2592000 => 'a.',
        604800 => 'h.',
        86400 => 'g.',
        3600 => 'sa.',
        60 => 'dk.',
        1 => 'sn.'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
    }

} ?>
    <?php include('../header.php');?>
        <?php include('../connect.php')?>
            <title>Tartışmalar</title>

            <body>

                <?php include('../navbar.php')?>
                    <div class="col-lg-3"></div>
                    <div class="container col-lg-6" id="content" style="margin-top:90">

                        
                            <?php

						   if($_GET['vote']=="yes")
							{   

								echo "

									<div class='alert alert-success'style='margin-top:1%;border:10px;'>
						<div class='container'>

						  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
							<span aria-hidden='true'><i class='fa fa-window-close'></i></span>
						  </button>
						  İmzalama Başarılı, ".$_COOKIE['userName'].".
						</div>
					  </div>

								";

							}
							?>
						
                            
                                    <h4 class="card-title" > <span style="color:#328CCC">Tartışmalar</span> 
						<small><a href="" style="float:right;" title="Oluştur" class="" data-toggle="modal" data-target="#exampleModal">Oluştur</a></small>
						</h4>
						
					
					
                                    <form action="createNoy" method="POST">
                                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"style="text-align:center">Yeni Tartışma oluştur</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class=" ">
                                                            <div class="">
                                                                <div class="row">
                                                                <div class=" col-lg-1">
                                                                    <img class="rounded-circle img-fluid" alt="Responsive image" width="45" src="../Kitaplık/Profilim/uploads/<?php echo $_COOKIE['userProfilePicture']; ?>" >
                                                                </div>
                                                                <div class="ml-3 col-lg-10">
                                                                    <div >
                                                                        <textarea type="text" name="description" maxlength="255" class="form-control" required autocomplete="off"rows="3"style="border-radius:10px;" /></textarea>
                                                        </div> 
                                                                    <div class="custom-control custom-checkbox" style="margin-top:2%;">
                                                                        <input type="checkbox" class="custom-control-input" id="defaultUnchecked"value="1"name="anonimity">
                                                                        <label class="custom-control-label" for="defaultUnchecked" >Anonim Olarak Paylaş</label>
                                                                        <label style="float:right;"><a href="../ToplulukKuralları"style="text-decoration:none;">Topluluk Kuralları</a></label>
                                                                    </div>
                                                                    
                                                                </div>
                                                                </div>
                                                              <div class="mt-3"style="float:right">  
                                                            <?php if($_COOKIE['userId']!=""){ ?>
                                                           
                                                            <button type="submit" class="btn btn-primary"> Paylaş</button>
                                                            <?php }else{?><a class="btn btn-outline-info">Paylaşmak için Giriş Yap</a>
                                                                <?php }?>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        
                                                        
                                                        
                                                        <script>
                                                            $(document).ready(function() {
                                                                $('[data-toggle="popover"]').popover();
                                                            });
                                                        </script>
                                                    </div>

                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    
                                    <?php 
                        $sql="SELECT g .*, u.name,u.schoolnumber,u.profilePictureUrl as profilePictureUrl FROM noy g LEFT JOIN user u ON g.userId = u.id ORDER BY created_at DESC";
                        $result=mysqli_query($db,$sql);
                        $count=mysqli_num_rows($result);
                        if($count==0){
                         echo "Yerinde yeller esiyor :( ";
                        }else{
                         while($row=mysqli_fetch_array($result)){
                         $noyId=$row['id'];
                         $counter=$row['noyCount'];
                         $goal=$row['goal'];
                         $userId=$_COOKIE['userId']; 
                         $goalratio=(100*$counter)/$goal;
                         $time = strtotime($row['created_at']);?>
                                        
                            <div class="card">
                                <div class="card-body">
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="mr-2">
                                                    <?php if($row['anonymity']==0){ ?>
                                                    <img class="rounded-circle" width="45" src="../Kitaplık/Profilim/uploads/<?php echo $row['profilePictureUrl']; ?>" alt="">
                                                    <?php } else{ ?>
                                                    <img class="rounded-circle" width="35" src="./anon.svg" alt="">
                                                    <?php } ?>
                                                </div>
                                                <div class="ml-2">
                                                    <div class="h6 m-0">
                                                        <?php if($row['anonymity']==0){ ?>
                                                                <a style="text-decoration:none"href="../Profil?id=<?php echo $row['userId'];?>"><?php echo $row['name'];?></a>
                                                         <?php }
                                                            else{?>
                                                                <a style="text-decoration:none"href="#">Anonim</a><?php } ?>
                                                    <span style="color:#96A3AE"><?php echo paylasilma_zamani($time); ?></span> 
                                                    
                                                    </div> 
                                                   <div class="text-muted h7 mb-2"> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                         <span class="ml-1"><?php echo $row['description']; ?></span>
                                         
                                        <?php if($_COOKIE['userId']){
                                        
                                        $sqlNoy="SELECT * FROM noyVoter where noy_id= '$noyId' AND user_id= '$userId'";
                                                    $resultNoy=mysqli_query($db,$sqlNoy);
                                                    $countNoy=mysqli_num_rows($resultNoy);?>
                                            <div class="row">
                                                <div class="col-lg-11 col-sm-11">
                                                    
                                                    <div id="hitDiv<?php echo $row['id'];?>">
                                                    <?php
                                                    /* loop the executed query */
                                                      if(mysqli_num_rows($resultNoy)==0){?><br>
                                                    		 <button class='btn  btn-outline-secondary' id='hitnOy<?php echo $row['id'];?>'><i class='fas fa-angle-up'></i> <?php echo $row['noyCount'];?></button>
                                                             <button class='btn  btn-outline-secondary'><i class='far fa-comments'></i> 0</button>
                		                            <input type="hidden"value="<?php echo $noyId;?>"id="noyId<?php echo $row['id'];?>"/>
                                        		   <?php } else{ ?><br>
                                        		              <button class='btn  btn-outline-success' id='hitnOy<?php echo $row['id'];?>'><i class='fas fa-angle-up'></i> <?php echo $counter;?> </button>
                                        		              <button class='btn  btn-outline-secondary'><i class='far fa-comments'></i> 0</button>
                		                                        <input type="hidden"value="<?php echo $noyId;?>"id="noyId<?php echo $row['id'];?>"/>
                                        		   <?php } ?>
                                        		    <script type="text/javascript" > 
            
                                                $(document).on("click", "#hitnOy<?php echo $row['id'];?>", function () {
                                                
                                                        jQuery("#hitDiv<?php echo $row['id'];?>").fadeIn(900, 0);   
                                                 /* load all variables */
                                                   var noyId = document.getElementById('noyId<?php echo $row['id'];?>').value              
                                                    $.ajax({    //create an ajax request to load_page.php
                                                        type:"POST",  
                                                        url: "upProcess",    
                                                        dataType: "text",   //expect html to be returned  
                                                        data:{noyId:noyId},          
                                                        success: function(data){                    
                                                         $("#hitDiv<?php echo $row['id'];?>").html(data); 
                                                          
                                                        }
                                                
                                                    }); 
                                                
                                                }); 
                                            </script>
                                            </div>
                                            
                                            
                                            
                                            
                                    
                                                </div>
                                                <!--<div class="col-lg-1">
                                                    
                                                    <a href="#" style="float:right" title="raporla"><i class="fa fa-lg fa-exclamation-circle"></i></a>
                                                    
                                                    <br>
                                                </div>-->
                                            </div>
                                            <?php }else{?>
                                            <br><br>
                                                <a href="../" class='btn  btn-outline-secondary' ><i class='fas fa-angle-up'></i> Desteklemek için giriş yap</a>
                                            <?php }?>
                                           
                                    </div>
                                 </div>
                                 <br>
                                            <?php }} ?>
                                                
                            

                    </div>
                    <div class="col-lg-3"></div>
                    <button onclick="topFunction()" id="myBtn" title="Yukarı çık"><i class="fas fa-chevron-circle-up"></i></button>
                    <script>
                        // When the user scrolls down 20px from the top of the document, show the button
                        window.onscroll = function() {
                            scrollFunction()
                        };

                        function scrollFunction() {
                            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                                document.getElementById("myBtn").style.display = "block";
                            } else {
                                document.getElementById("myBtn").style.display = "none";
                            }
                        }

                        // When the user clicks on the button, scroll to the top of the document
                        function topFunction() {
                            document.body.scrollTop = 0; // For Safari
                            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
                        }
                    </script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
            </body>