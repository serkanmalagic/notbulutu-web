<?php

    date_default_timezone_set('Europe/Istanbul');

function paylasilma_zamani ($time)
{

    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => 'yıl',
        2592000 => 'ay',
        604800 => 'hafta',
        86400 => 'gün',
        3600 => 'saat',
        60 => 'dakika',
        1 => 'saniye'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'':'');
    }

}

include('connect.php');
session_start();
$userId=$_COOKIE['userId'];

if(isset($_POST['view'])){

// $db = mysqli_connect("localhost", "root", "", "notif");

if($_POST["view"] != '')
{
    $update_query = "UPDATE notification SET notification_status = 1 WHERE notification_status=0";
    mysqli_query($db, $update_query);
}
$query = "SELECT * FROM notification WHERE userId='$userId' OR userId='*' ORDER BY id DESC ";
$result = mysqli_query($db, $query);
$output = '';
if(mysqli_num_rows($result) > 0)
{
 while($row = mysqli_fetch_array($result))
 {
    $time = strtotime($row['created_at']);
    $text = paylasilma_zamani($time);
    $output .= '
    <li>
                                            <p><a href="#"style="text-decoration:none;color:#333333">
                                                <span class="timeline-advenced timeline-title"><b>'.$row["notification_subject"].'</b></span><br> <span class="timeline-subtitle">'.$row["notification_text"].'</span>
                                                <span class="timeline-icon"><i class="fas fa-sm fa-cloud"style="color:#328CCC"></i></span>
                                                <span class="timeline-date">'.$text.' önce</span></a>
                                            </p>
                                        </li>

                          
   ';

 }
}
else{
     $output .= '
      <a class="dropdown-item" href="#">Bildirim bulunamadı</a>';
}



$status_query = "SELECT * FROM notification WHERE notification_status=0";
$result_query = mysqli_query($db, $status_query);
$count = mysqli_num_rows($result_query);
$data = array(
    'notification' => $output,
    'unseen_notification'  => $count
);

echo json_encode($data);

}

?>