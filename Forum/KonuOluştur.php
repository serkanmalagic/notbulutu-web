<?php include('../header.php')?>
<?php include("../connect.php");?>
<body>
    <style>
        #myBtn {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 20px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #328CCC; /* Set a background color */
    color: white; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    padding: 15px; /* Some padding */
    border-radius: 10px; /* Rounded corners */
    font-size: 18px; /* Increase font size */
}

#myBtn:hover {
    background-color: white; /* Add a dark-grey background on hover */
    color: #328CCC;
}
#myInput {
  background-image: url('/css/searchicon.png'); /* Add a search icon to input */
  background-position: 10px 12px; /* Position the search icon */
  background-repeat: no-repeat; /* Do not repeat the icon image */
  width: 100%; /* Full-width */
  font-size: 16px; /* Increase font-size */
  padding: 12px 20px 12px 40px; /* Add some padding */
  border: 1px solid #ddd; /* Add a grey border */
  margin-bottom: 12px; /* Add some space below the input */
}
		.inputt {
            border: 2px solid #CCCCCC;
            border-radius: 8px 8px 8px 8px;
          
          margin-top:1%;
            outline: medium none;
            padding: 8px 12px;
            width: 100%;
        }

#myTable {
  border-collapse: collapse; /* Collapse borders */
  width: 100%; /* Full-width */
  border: 1px solid #ddd; /* Add a grey border */
  font-size: 18px; /* Increase font-size */
}

#myTable th, #myTable td {
  text-align: left; /* Left-align text */
  padding: 12px; /* Add padding */
}

#myTable tr {
  /* Add a bottom border to all table rows */
  border-bottom: 1px solid #ddd; 
}

#myTable tr.header, #myTable tr:hover {
  /* Add a grey background color to the table header and on hover */
  background-color: #f1f1f1;
}
</style>
<?php include('../navbar.php')?>
<div class="row"style="margin-top:120px;">

 <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <h1><small>Konu Oluştur</small></h1><br>
        <label>Kategoriler*</label>
        <select class="inputt">
        	<option>Kategori Seçiniz</option>
        	<option>Dersler</option>
        	<option>Hocalar</option>
        	<option>Sınavlar</option>
        	<option>GED</option>
        	<option>NOY</option>
        </select><br><br>
        <label>Konu Başlığı*</label>
		<input type="text"class="inputt"placeholder="" autocomplete="off"autofocus/><hr>
		<label>Açıklama*</label>
		<textarea placeholder="" class="inputt"rows="10"></textarea>

<button type="submit" class="btn btn-lg btn-info">PAYLAŞ</button>



<div>
 <div class="col-lg-2"></div>

<button onclick="topFunction()" id="myBtn" title="Yukarı çık"><i class="fas fa-chevron-circle-up"></i></button>
<script>// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}</script>
<script>
function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
</script>
</body>